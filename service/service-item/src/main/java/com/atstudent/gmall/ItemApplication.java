package com.atstudent.gmall;

import com.atstudent.gmall.common.anno.EnableThreadPool;
import com.atstudent.gmall.common.cache.anno.EnableRedissonClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@EnableFeignClients(basePackages = {
        "com.atstudent.gmall.common.feign.product",
        "com.atstudent.gmall.common.feign.search"
})
@EnableRedissonClient
@EnableThreadPool
public class ItemApplication {

    public static void main(String[] args) {
        SpringApplication.run(ItemApplication.class , args) ;
    }

}
