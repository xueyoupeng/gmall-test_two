package com.atstudent.gmall.biz.impl;/*
 * @author: XueYouPeng
 * @time: 23.9.7 下午 12:19
 */

import com.atstudent.gmall.biz.SkuDetailBizService;
import com.atstudent.gmall.common.cache.anno.GmallCache;
import com.atstudent.gmall.common.constant.GmallConstant;
import com.atstudent.gmall.common.feign.product.SkuFeignClient;
import com.atstudent.gmall.common.feign.search.SearchFeignClient;
import com.atstudent.gmall.product.vo.SkuDetailVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;


import java.util.concurrent.TimeUnit;

@Service
public class SkuDetailBizServiceImpl implements SkuDetailBizService {

    @Autowired
    private SkuFeignClient skuFeignClient ;

    @Autowired
    private SearchFeignClient searchFeignClient;

    @Autowired
    private RedisTemplate<String,String> redisTemplate;

    @Override
    @GmallCache(
            cacheKey = GmallConstant.REDSI_SKU_DETAIL_PREFIX + "#{#params[0]}",
            bloomFilterName = GmallConstant.REDIS_SKUID_BLOOM_FILTER ,
            bloomFilterValue = "#{#params[0]}" ,
            enableLock = true ,
            lockName = GmallConstant.REDIS_ITEM_LOCK_PREFIX + "#{#params[0]}" ,
            time = 7 ,
            timeUnit = TimeUnit.DAYS
    )
    public SkuDetailVo item(Long skuId) {

        SkuDetailVo skuDetailVo = skuFeignClient.findSkuDetailVo(skuId).getData();

        return skuDetailVo;

        //        //调用product服务中的接口
//        //根据skuId查询三级分类数据
//        Result<CategoryView> viewResult = skuFeignClient.findCategoryViewBySkuId(skuId);
//        CategoryView categoryView = viewResult.getData();
//
//        //根据skuid查询sku的基本信息和图片信息
//        Result<SkuInfo> skuInfoAndImageResult = skuFeignClient.findSkuInfoAndImageBySkuId(skuId);
//        SkuInfo skuInfoAndImage = skuInfoAndImageResult.getData();
//
//        //根据skuid查询价格数据
//        Result<SkuInfo> skuInfoResult = skuFeignClient.findSkuInfoBySkuId(skuId);
//        BigDecimal price = skuInfoResult.getData().getPrice();
//
//        //根据skuid查询所对应的spu的销售属性和销售属性值
//        Result<List<SpuSaleAttr>> spuSalAttrResult = skuFeignClient.findSpuSalAttrBySkuId(skuId);
//        List<SpuSaleAttr> spuSaleAttrList = spuSalAttrResult.getData();
//
//        //根据skuid查询出该sku所对应的所有兄弟sku（包含自己）的销售属性值的组合
//        Result<List<AttrValueConcatVo>> listResult = skuFeignClient.findSkuAttrValueConcatBySkuId(skuId);
//        List<AttrValueConcatVo> data = listResult.getData();
//        Map<String, Long> map = data.stream().collect(Collectors.toMap(attrValueConcatVo -> attrValueConcatVo.getAttrValueConcat(), attrValueConcatVo -> attrValueConcatVo.getSkuId()));
//        String toJSONString = JSON.toJSONString(map);
//
//
//        //创建skuDetailVo对象，封装查询结果数据
//        SkuDetailVo skuDetailVo = new SkuDetailVo();
//        skuDetailVo.setCategoryView(categoryView);
//        skuDetailVo.setSkuInfo(skuInfoAndImage);
//        skuDetailVo.setPrice(price);
//        skuDetailVo.setSpuSaleAttrList(spuSaleAttrList);
//        skuDetailVo.setValuesSkuJson(toJSONString);
    }

    @Override
    public void updateHotScore(Long skuId) {
        //访问一次就在Redis中设置一次 +1
        Long increment = redisTemplate.opsForValue().increment(GmallConstant.REDIS_SKU_HOT_SCORE_PRE + skuId);
        if (increment % 100 == 0){
            searchFeignClient.updateHotScore(skuId , increment);
        }
    }
}
