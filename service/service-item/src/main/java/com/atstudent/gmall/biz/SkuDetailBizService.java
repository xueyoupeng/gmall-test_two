package com.atstudent.gmall.biz;/*
 * @author: XueYouPeng
 * @time: 23.9.7 下午 12:19
 */

import com.atstudent.gmall.product.vo.SkuDetailVo;

public interface SkuDetailBizService {
    public abstract SkuDetailVo item(Long skuId);

    public abstract void updateHotScore(Long skuId);
}
