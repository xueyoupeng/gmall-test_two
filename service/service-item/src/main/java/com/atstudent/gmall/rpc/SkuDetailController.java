package com.atstudent.gmall.rpc;/*
 * @author: XueYouPeng
 * @time: 23.9.7 下午 12:19
 */

import com.atstudent.gmall.biz.SkuDetailBizService;
import com.atstudent.gmall.common.result.Result;
import com.atstudent.gmall.common.result.ResultCodeEnum;
import com.atstudent.gmall.product.vo.SkuDetailVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/inner/item")
public class SkuDetailController {

    @Autowired
    private SkuDetailBizService skuDetailBizService;



    @GetMapping(value = "/item/{skuId}")
    public Result<SkuDetailVo> item(@PathVariable(value = "skuId") Long skuId){

        SkuDetailVo skuDetailVo = skuDetailBizService.item(skuId);

        skuDetailBizService.updateHotScore(skuId);

        return Result.build(skuDetailVo, ResultCodeEnum.SUCCESS);

    }

}
