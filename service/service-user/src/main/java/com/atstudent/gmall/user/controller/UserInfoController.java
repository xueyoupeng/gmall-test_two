package com.atstudent.gmall.user.controller;/*
 * @author: XueYouPeng
 * @time: 23.9.13 下午 5:25
 */

import com.atstudent.gmall.common.result.Result;
import com.atstudent.gmall.common.result.ResultCodeEnum;
import com.atstudent.gmall.user.dto.UserLoginDto;
import com.atstudent.gmall.user.service.UserInfoService;
import com.atstudent.gmall.user.vo.UserLoginSuccessVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/user")
public class UserInfoController {

    @Autowired
    private UserInfoService userInfoService;

    @PostMapping(value = "/passport/login")
    public Result<UserLoginSuccessVo> login(@RequestBody UserLoginDto userLoginDto) {
        UserLoginSuccessVo userLoginSuccessVo = userInfoService.login(userLoginDto);
        return Result.build(userLoginSuccessVo , ResultCodeEnum.SUCCESS);
    }

    @GetMapping(value = "/passport/logout")
    public Result logout(@RequestHeader(value = "token") String token){
        userInfoService.logout(token);
        return Result.ok();
    }
}
