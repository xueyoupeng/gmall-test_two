package com.atstudent.gmall.user.rpc;/*
 * @author: XueYouPeng
 * @time: 23.9.16 下午 6:37
 */

import com.atstudent.gmall.common.result.Result;
import com.atstudent.gmall.common.result.ResultCodeEnum;
import com.atstudent.gmall.user.biz.UserAddressBizService;
import com.atstudent.gmall.user.entity.UserAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api/inner/user")
public class UserAddressRpcController {

    @Autowired
    private UserAddressBizService userAddressBizService;

    /*
     * 查询用户对应的地址信息
     * @author: XueYouPeng
     * @time: 23.9.16 下午 8:09
     */
    @GetMapping(value = "/findUserAddressByUserId")
    public Result<List<UserAddress>>findUserAddressByUserId(){
        List<UserAddress> userAddressList = userAddressBizService.findUserAddressByUserId();
        return Result.build(userAddressList, ResultCodeEnum.SUCCESS);
    }
}
