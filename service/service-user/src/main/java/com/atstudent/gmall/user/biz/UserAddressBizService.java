package com.atstudent.gmall.user.biz;/*
 * @author: XueYouPeng
 * @time: 23.9.16 下午 6:37
 */

import com.atstudent.gmall.user.entity.UserAddress;

import java.util.List;

public interface UserAddressBizService {
    public abstract List<UserAddress> findUserAddressByUserId();
}
