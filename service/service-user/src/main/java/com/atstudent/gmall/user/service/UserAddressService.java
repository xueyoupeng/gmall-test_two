package com.atstudent.gmall.user.service;

import com.atstudent.gmall.user.entity.UserAddress;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author xueyoupeng
* @description 针对表【user_address(用户地址表)】的数据库操作Service
* @createDate 2023-09-13 16:11:09
*/
public interface UserAddressService extends IService<UserAddress> {

}
