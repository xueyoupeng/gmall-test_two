package com.atstudent.gmall.user.biz.impl;/*
 * @author: XueYouPeng
 * @time: 23.9.16 下午 6:37
 */

import com.atstudent.gmall.common.feign.interceptor.AuthUserInfo;
import com.atstudent.gmall.common.feign.util.AuthUserInfoUtils;
import com.atstudent.gmall.user.biz.UserAddressBizService;
import com.atstudent.gmall.user.entity.UserAddress;
import com.atstudent.gmall.user.mapper.UserAddressMapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserAddressBizServiceImpl implements UserAddressBizService {

    @Autowired
    private UserAddressMapper userAddressMapper;

    @Override
    public List<UserAddress> findUserAddressByUserId() {

        //获取当前登录的用户的id
        AuthUserInfo authUserInfo = AuthUserInfoUtils.getAuthUserInfo();
        String userIdStr = authUserInfo.getUserId();
        //转换成Long类型
        long userId = Long.parseLong(userIdStr);

        //根据userId查询收货人地址
        LambdaQueryWrapper<UserAddress> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(UserAddress::getUserId , userId);

        List<UserAddress> userAddressList = userAddressMapper.selectList(queryWrapper);
        return userAddressList;
    }
}
