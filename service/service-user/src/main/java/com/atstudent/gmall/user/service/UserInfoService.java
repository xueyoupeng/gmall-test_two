package com.atstudent.gmall.user.service;

import com.atstudent.gmall.user.dto.UserLoginDto;
import com.atstudent.gmall.user.entity.UserInfo;
import com.atstudent.gmall.user.vo.UserLoginSuccessVo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author xueyoupeng
* @description 针对表【user_info(用户表)】的数据库操作Service
* @createDate 2023-09-13 16:11:09
*/
public interface UserInfoService extends IService<UserInfo> {

    public abstract UserLoginSuccessVo login(UserLoginDto userLoginDto);

    public abstract void logout(String token);
}
