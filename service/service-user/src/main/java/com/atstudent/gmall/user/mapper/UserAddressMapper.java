package com.atstudent.gmall.user.mapper;

import com.atstudent.gmall.user.entity.UserAddress;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author xueyoupeng
* @description 针对表【user_address(用户地址表)】的数据库操作Mapper
* @createDate 2023-09-13 16:11:09
* @Entity com.atstudent.gmall.user.entity.UserAddress
*/
public interface UserAddressMapper extends BaseMapper<UserAddress> {

}




