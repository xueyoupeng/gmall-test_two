package com.atstudent.gmall.user.service.impl;

import com.alibaba.fastjson.JSON;
import com.atstudent.gmall.common.constant.GmallConstant;
import com.atstudent.gmall.common.execption.GmallException;
import com.atstudent.gmall.common.result.ResultCodeEnum;
import com.atstudent.gmall.user.dto.UserLoginDto;
import com.atstudent.gmall.user.vo.UserLoginSuccessVo;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atstudent.gmall.user.entity.UserInfo;
import com.atstudent.gmall.user.service.UserInfoService;
import com.atstudent.gmall.user.mapper.UserInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.util.UUID;

/**
* @author xueyoupeng
* @description 针对表【user_info(用户表)】的数据库操作Service实现
* @createDate 2023-09-13 16:11:09
*/
@Service
public class UserInfoServiceImpl extends ServiceImpl<UserInfoMapper, UserInfo>
    implements UserInfoService{

    @Autowired
    private UserInfoMapper userInfoMapper;

    @Autowired
    private RedisTemplate<String , String> redisTemplate;

    @Override
    public UserLoginSuccessVo login(UserLoginDto userLoginDto) {

        //从数据库中校验是否存在这个用户
//        QueryWrapper<UserInfo> wrapper = new QueryWrapper<>();
//        wrapper.eq("loginName" , userLoginDto.getLoginName());
        LambdaQueryWrapper<UserInfo> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(UserInfo::getLoginName , userLoginDto.getLoginName());
        UserInfo userInfo = userInfoMapper.selectOne(wrapper);
        if (userInfo == null){
            throw new GmallException(ResultCodeEnum.LOGIN_ERROR);
        }

        //校验密码
        String passwd = userLoginDto.getPasswd();
        //调用工具类 对密码进行md5加密
        String md5Passwd = DigestUtils.md5DigestAsHex(passwd.getBytes());
        if (!md5Passwd.equals(userInfo.getPasswd())){
            throw new GmallException(ResultCodeEnum.LOGIN_ERROR);
        }

        //密码正确 生成token令牌
        String token = UUID.randomUUID().toString().replace("-", "");
        //前缀+token最为key 用户信息作为value存储到Redis中
        redisTemplate.opsForValue().set(GmallConstant.REDIS_USER_LOGIN_PRE + token , JSON.toJSONString(userInfo));

        //构建响应结果
        UserLoginSuccessVo userLoginSuccessVo = new UserLoginSuccessVo();
        userLoginSuccessVo.setToken(token);
        userLoginSuccessVo.setNickName(userInfo.getNickName());

        return userLoginSuccessVo;
    }

    @Override
    public void logout(String token) {
        //从Redis中删除用户数据 key就是前缀+token
        redisTemplate.delete(GmallConstant.REDIS_USER_LOGIN_PRE + token);
    }
}




