package com.atstudent.gmall.order.rpc;/*
 * @author: XueYouPeng
 * @time: 23.9.16 下午 6:17
 */

import com.atstudent.gmall.common.result.Result;
import com.atstudent.gmall.common.result.ResultCodeEnum;
import com.atstudent.gmall.order.biz.OrderBizService;
import com.atstudent.gmall.order.entity.OrderInfo;
import com.atstudent.gmall.order.vo.OrderConfirmVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/inner/order")
public class OrderRpcController {

    @Autowired
    private OrderBizService orderBizService;

    @GetMapping(value = "/submitOrder")
    public Result<OrderConfirmVo> submitOrder(){
        OrderConfirmVo orderConfirmVo = orderBizService.submitOrder();
        return Result.build(orderConfirmVo , ResultCodeEnum.SUCCESS);
    }

    @GetMapping(value = "/findOrderInfo/{orderId}")
    public Result<OrderInfo> findOrderInfo(@PathVariable(value = "orderId") Long orderId){
        OrderInfo orderInfo = orderBizService.findOrderInfo(orderId);
        return Result.build(orderInfo , ResultCodeEnum.SUCCESS) ;
    }
}
