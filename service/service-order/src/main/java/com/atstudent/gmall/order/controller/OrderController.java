package com.atstudent.gmall.order.controller;/*
 * @author: XueYouPeng
 * @time: 23.9.17 下午 2:27
 */

import com.atstudent.gmall.common.result.Result;
import com.atstudent.gmall.common.result.ResultCodeEnum;
import com.atstudent.gmall.order.dto.OrderSubmitDto;
import com.atstudent.gmall.order.service.OrderInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/api/order")
public class OrderController {

    @Autowired
    private OrderInfoService orderInfoService ;

    /**
     * @Valid注解使用到实体类型参数的前面，表示该实体类中的相关属性需要开启校验功能
     * @param tradeNo
     * @param orderSubmitDto
     * @return
     */
    @PostMapping(value = "/auth/submitOrder")
    public Result<String>  submitOrder(@RequestParam(value = "tradeNo") String tradeNo ,@Valid @RequestBody OrderSubmitDto orderSubmitDto) {
        String orderId = orderInfoService.submitOrder(tradeNo , orderSubmitDto) ;
        return Result.build(orderId , ResultCodeEnum.SUCCESS) ;
    }
}
