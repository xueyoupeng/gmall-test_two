package com.atstudent.gmall.order.biz;/*
 * @author: XueYouPeng
 * @time: 23.9.16 下午 6:18
 */

import com.atstudent.gmall.order.entity.OrderInfo;
import com.atstudent.gmall.order.vo.OrderConfirmVo;

public interface OrderBizService {
    public abstract OrderConfirmVo submitOrder();

    public abstract OrderInfo findOrderInfo(Long orderId);
}
