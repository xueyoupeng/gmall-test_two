package com.atstudent.gmall.order.service;

import com.atstudent.gmall.order.dto.OrderSubmitDto;
import com.atstudent.gmall.order.entity.OrderInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author xueyoupeng
* @description 针对表【order_info(订单表 订单表)】的数据库操作Service
* @createDate 2023-09-16 15:00:31
*/
public interface OrderInfoService extends IService<OrderInfo> {


    public abstract String submitOrder(String tradeNo, OrderSubmitDto orderSubmitDto);

    public abstract void closeOrder(Long orderId, Long userId) ;
}
