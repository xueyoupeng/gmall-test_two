package com.atstudent.gmall.order.service;

import com.atstudent.gmall.order.entity.OrderStatusLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author xueyoupeng
* @description 针对表【order_status_log】的数据库操作Service
* @createDate 2023-09-16 15:00:31
*/
public interface OrderStatusLogService extends IService<OrderStatusLog> {

}
