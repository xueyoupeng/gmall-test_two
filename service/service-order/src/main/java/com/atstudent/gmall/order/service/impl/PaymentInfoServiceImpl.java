package com.atstudent.gmall.order.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atstudent.gmall.order.entity.PaymentInfo;
import com.atstudent.gmall.order.service.PaymentInfoService;
import com.atstudent.gmall.order.mapper.PaymentInfoMapper;
import org.springframework.stereotype.Service;

/**
* @author xueyoupeng
* @description 针对表【payment_info(支付信息表)】的数据库操作Service实现
* @createDate 2023-09-16 15:00:31
*/
@Service
public class PaymentInfoServiceImpl extends ServiceImpl<PaymentInfoMapper, PaymentInfo>
    implements PaymentInfoService{

}




