package com.atstudent.gmall.order.service;

import com.atstudent.gmall.order.entity.OrderDetail;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author xueyoupeng
* @description 针对表【order_detail(订单明细表)】的数据库操作Service
* @createDate 2023-09-16 15:00:31
*/
public interface OrderDetailService extends IService<OrderDetail> {

}
