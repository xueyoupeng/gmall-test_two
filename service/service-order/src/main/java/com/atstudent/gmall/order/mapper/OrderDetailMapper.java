package com.atstudent.gmall.order.mapper;

import com.atstudent.gmall.order.entity.OrderDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author xueyoupeng
* @description 针对表【order_detail(订单明细表)】的数据库操作Mapper
* @createDate 2023-09-16 15:00:31
* @Entity com.atstudent.gmall.order.entity.OrderDetail
*/
public interface OrderDetailMapper extends BaseMapper<OrderDetail> {

}




