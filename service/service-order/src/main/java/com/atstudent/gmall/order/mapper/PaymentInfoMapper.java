package com.atstudent.gmall.order.mapper;

import com.atstudent.gmall.order.entity.PaymentInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author xueyoupeng
* @description 针对表【payment_info(支付信息表)】的数据库操作Mapper
* @createDate 2023-09-16 15:00:31
* @Entity com.atstudent.gmall.order.entity.PaymentInfo
*/
public interface PaymentInfoMapper extends BaseMapper<PaymentInfo> {

}




