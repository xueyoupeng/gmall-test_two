package com.atstudent.gmall.order;

import com.atstudent.gmall.common.feign.anno.EnableFeignClientInterceptor;
import com.atstudent.gmall.rabbit.anno.EnableOrderDelayQueue;
import com.atstudent.gmall.rabbit.anno.EnableRabbit;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@MapperScan(basePackages = "com.atstudent.gmall.order.mapper")
@EnableFeignClients(basePackages = {
        "com.atstudent.gmall.common.feign.user",
        "com.atstudent.gmall.common.feign.cart",
        "com.atstudent.gmall.common.feign.product",
        "com.atstudent.gmall.common.feign.ware"
})
//使用feign远程调用时 要加入此自定义注解 过滤器 来实现id透传（默认请求头是会丢失的）
@EnableFeignClientInterceptor
@EnableRabbit
@EnableOrderDelayQueue
public class OrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(OrderApplication.class , args) ;
    }

}
