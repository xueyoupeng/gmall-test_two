package com.atstudent.gmall.order.service;

import com.atstudent.gmall.order.entity.PaymentInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author xueyoupeng
* @description 针对表【payment_info(支付信息表)】的数据库操作Service
* @createDate 2023-09-16 15:00:31
*/
public interface PaymentInfoService extends IService<PaymentInfo> {

}
