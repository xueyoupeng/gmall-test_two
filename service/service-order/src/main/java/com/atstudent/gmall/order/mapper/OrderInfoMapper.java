package com.atstudent.gmall.order.mapper;

import com.atstudent.gmall.order.entity.OrderInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author xueyoupeng
* @description 针对表【order_info(订单表 订单表)】的数据库操作Mapper
* @createDate 2023-09-16 15:00:31
* @Entity com.atstudent.gmall.order.entity.OrderInfo
*/
public interface OrderInfoMapper extends BaseMapper<OrderInfo> {

}




