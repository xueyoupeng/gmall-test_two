package com.atstudent.gmall.order.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atstudent.gmall.order.entity.OrderDetail;
import com.atstudent.gmall.order.service.OrderDetailService;
import com.atstudent.gmall.order.mapper.OrderDetailMapper;
import org.springframework.stereotype.Service;

/**
* @author xueyoupeng
* @description 针对表【order_detail(订单明细表)】的数据库操作Service实现
* @createDate 2023-09-16 15:00:31
*/
@Service
public class OrderDetailServiceImpl extends ServiceImpl<OrderDetailMapper, OrderDetail>
    implements OrderDetailService{

}




