package com.atstudent.gmall.order.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atstudent.gmall.order.entity.OrderStatusLog;
import com.atstudent.gmall.order.service.OrderStatusLogService;
import com.atstudent.gmall.order.mapper.OrderStatusLogMapper;
import org.springframework.stereotype.Service;

/**
* @author xueyoupeng
* @description 针对表【order_status_log】的数据库操作Service实现
* @createDate 2023-09-16 15:00:31
*/
@Service
public class OrderStatusLogServiceImpl extends ServiceImpl<OrderStatusLogMapper, OrderStatusLog>
    implements OrderStatusLogService{

}




