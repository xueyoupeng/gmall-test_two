package com.atstudent.gmall.order.mapper;

import com.atstudent.gmall.order.entity.OrderStatusLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author xueyoupeng
* @description 针对表【order_status_log】的数据库操作Mapper
* @createDate 2023-09-16 15:00:31
* @Entity com.atstudent.gmall.order.entity.OrderStatusLog
*/
public interface OrderStatusLogMapper extends BaseMapper<OrderStatusLog> {

}




