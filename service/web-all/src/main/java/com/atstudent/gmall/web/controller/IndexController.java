package com.atstudent.gmall.web.controller;/*
 * @author: XueYouPeng
 * @time: 23.9.6 下午 8:43
 */

import com.atstudent.gmall.common.feign.product.BaseCategoryFeignClient;
import com.atstudent.gmall.common.result.Result;
import com.atstudent.gmall.product.vo.CategoryVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class IndexController {

    @Autowired
    private BaseCategoryFeignClient baseCategoryFeignClient;


    @GetMapping(value = {"/","/index.html"})
    public String index(Model model){
        Result<List<CategoryVo>> result = baseCategoryFeignClient.findAllCategoryTree();

        //查询到的结果封装到model数据模型中
        List<CategoryVo> categoryVoList = result.getData();
        model.addAttribute("list",categoryVoList);

        return "index/index";
    }
}
