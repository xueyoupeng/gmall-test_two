package com.atstudent.gmall.web;/*
 * @author: XueYouPeng
 * @time: 23.7.14 下午 6:48
 */

import com.atstudent.gmall.common.feign.anno.EnableFeignClientInterceptor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@EnableFeignClients(basePackages ={
        "com.atstudent.gmall.common.feign.product",
        "com.atstudent.gmall.common.feign.item",
        "com.atstudent.gmall.common.feign.search",
        "com.atstudent.gmall.common.feign.cart",
        "com.atstudent.gmall.common.feign.order"
})
@EnableFeignClientInterceptor
public class WebApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebApplication.class,args);
    }
}
