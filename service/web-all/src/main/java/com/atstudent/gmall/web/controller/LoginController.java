package com.atstudent.gmall.web.controller;/*
 * @author: XueYouPeng
 * @time: 23.9.13 下午 3:50
 */


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class LoginController {


    //@RequestParam用于从请求的查询参数或表单数据中获取参数值。
    //参数值通过键值对的形式传递，如?key=value。
    //@PathVariable用于从请求的URL路径中获取参数值。
    ///如users/{id}。
    //originUrl 代表从哪个页面进行登录 这个url就是那个页面的url 登陆成功后，又跳回到这个页面
    @GetMapping(value = "/login.html")
    public String login(@RequestParam(value = "originUrl") String originUrl, Model model){
        model.addAttribute("originUrl" , originUrl);
        return "login";

    }
}
