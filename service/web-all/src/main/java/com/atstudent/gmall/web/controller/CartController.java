package com.atstudent.gmall.web.controller;/*
 * @author: XueYouPeng
 * @time: 23.9.14 下午 10:18
 */

import com.atstudent.gmall.cart.vo.AddCartSuccessVo;
import com.atstudent.gmall.common.feign.cart.CartFeignClient;
import com.atstudent.gmall.common.result.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@Slf4j
public class CartController {

    @Autowired
    private CartFeignClient cartFeignClient;

    @GetMapping(value = "/addCart.html")
    public String addCart(
            @RequestParam(value = "skuId") Long skuId,
            @RequestParam(value = "skuNum") Integer skuNum,
            Model model
    ){

        //调用feign接口中的添加到购物车 的接口
        Result<AddCartSuccessVo> result = cartFeignClient.addCart(skuId, skuNum);
        AddCartSuccessVo addCartSuccessVo = result.getData();
        if (addCartSuccessVo == null){
            return "cart/addCartError" ;
        }
        //从后端接口中拿到的数据封装到model模型中
        model.addAttribute("skuInfo" , addCartSuccessVo.getSkuInfo());
        model.addAttribute("skuNum" , addCartSuccessVo.getSkuNum());

        return "cart/addCart";
    }

    //跳转到购物车页面
    @GetMapping(value = "/cart.html")
    public String cart() {
        return "cart/index" ;
    }

    //删除选中的商品之后 跳转到购物车页面
    @GetMapping(value = "/cart/deleteChecked")
    public String deleteChecked(){
        cartFeignClient.deleteAllChecked();
        return "cart/index";
    }
}
