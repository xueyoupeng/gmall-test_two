package com.atstudent.gmall.web.controller;/*
 * @author: XueYouPeng
 * @time: 23.9.16 下午 6:14
 */

import com.atstudent.gmall.common.feign.order.OrderFeignClient;
import com.atstudent.gmall.common.result.Result;
import com.atstudent.gmall.order.vo.OrderConfirmVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class OrderController {

    @Autowired
    private OrderFeignClient orderFeignClient;

    @GetMapping(value = "/trade.html")
    public String trade(Model model){

        //远程调用order的接口
        Result<OrderConfirmVo> confirmVoResult = orderFeignClient.submitOrder();
        OrderConfirmVo orderConfirmVo = confirmVoResult.getData();

        // 获取数据存储到Model数据模型中
        model.addAttribute("detailArrayList",orderConfirmVo.getDetailArrayList());
        model.addAttribute("totalNum",orderConfirmVo.getTotalNum());
        model.addAttribute("totalAmount" , orderConfirmVo.getTotalAmount());
        model.addAttribute("userAddressList" , orderConfirmVo.getUserAddressList());
        model.addAttribute("tradeNo" , orderConfirmVo.getTradeNo());

        //跳转页面
        return "order/trade";
    }

}
