package com.atstudent.gmall.search.biz;/*
 * @author: XueYouPeng
 * @time: 23.9.12 下午 1:59
 */

import com.atstudent.gmall.search.dto.SearchParamDTO;
import com.atstudent.gmall.search.entity.Goods;
import com.atstudent.gmall.search.vo.SearchResponseVo;

public interface GoodsBizService {
    public abstract void saveGoods(Goods goods);

    public abstract void deleteById(Long skuId);

    public abstract SearchResponseVo search(SearchParamDTO searchParamDTO);

    public abstract void updateHotScore(Long skuId, Long hotScore);
}
