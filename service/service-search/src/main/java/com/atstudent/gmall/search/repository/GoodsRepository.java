package com.atstudent.gmall.search.repository;/*
 * @author: XueYouPeng
 * @time: 23.9.12 下午 1:54
 */

import com.atstudent.gmall.search.entity.Goods;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface GoodsRepository extends ElasticsearchRepository<Goods, Long> {
}
