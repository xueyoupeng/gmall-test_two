package com.atstudent.gmall.pay.service;/*
 * @author: XueYouPeng
 * @time: 23.9.19 上午 10:50
 */

public interface PayService {
    String pay(Long orderId);
}
