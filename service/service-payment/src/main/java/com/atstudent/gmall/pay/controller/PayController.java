package com.atstudent.gmall.pay.controller;/*
 * @author: XueYouPeng
 * @time: 23.9.19 上午 10:49
 */

import com.atstudent.gmall.pay.service.PayService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/payment")
@Slf4j
public class PayController {

    @Autowired
    private PayService payService;

    @GetMapping(value = "/alipay/submit/{orderId}")
    public String alipay(@PathVariable(value = "orderId") Long orderId) {
        log.info("PayController...alipay方法执行了...");
        String result = payService.pay(orderId) ;
        return result ;
    }
}
