package com.atstudent.gmall.pay.properties;/*
 * @author: XueYouPeng
 * @time: 23.9.19 下午 2:23
 */

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "gmall.alipay")
public class AliPayProperties {

    private String     gateway ;
    private String     appId ;
    private String     privateKey ;
    private String     format ;
    private String     charset ;
    private String     publicKey ;
    private String     signType ;
    private String     notifyUrl ;
    private String     returnUrl ;
}
