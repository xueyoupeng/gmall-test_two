package com.atstudent.gmall.pay;


import com.atstudent.gmall.common.feign.anno.EnableFeignClientInterceptor;
import com.atstudent.gmall.pay.properties.AliPayProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@EnableConfigurationProperties(value = AliPayProperties.class)
@EnableFeignClients(basePackages = {
        "com.atstudent.gmall.common.feign.order"
})
@EnableFeignClientInterceptor

public class PayApplication {

    public static void main(String[] args) {
        SpringApplication.run(PayApplication.class , args) ;
    }

}
