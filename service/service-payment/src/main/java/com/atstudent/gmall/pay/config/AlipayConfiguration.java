package com.atstudent.gmall.pay.config;/*
 * @author: XueYouPeng
 * @time: 23.9.19 下午 2:22
 */

import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.atstudent.gmall.pay.properties.AliPayProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AlipayConfiguration {

    @Autowired
    private AliPayProperties aliPayProperties;

    @Bean
    public AlipayClient alipayClient(){

        DefaultAlipayClient alipayClient = new DefaultAlipayClient(aliPayProperties.getGateway(), aliPayProperties.getAppId(),
                aliPayProperties.getPrivateKey(),
                aliPayProperties.getFormat(), aliPayProperties.getCharset(), aliPayProperties.getPublicKey(),
                aliPayProperties.getSignType());
        return alipayClient;
    }
}
