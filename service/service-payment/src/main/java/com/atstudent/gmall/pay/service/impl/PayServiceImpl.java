package com.atstudent.gmall.pay.service.impl;/*
 * @author: XueYouPeng
 * @time: 23.9.19 上午 10:51
 */

import com.alibaba.fastjson.JSON;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.atstudent.gmall.common.feign.order.OrderFeignClient;
import com.atstudent.gmall.common.result.Result;
import com.atstudent.gmall.order.entity.OrderInfo;
import com.atstudent.gmall.pay.properties.AliPayProperties;
import com.atstudent.gmall.pay.service.PayService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class PayServiceImpl implements PayService {

    @Autowired
    private AliPayProperties aliPayProperties;

    @Autowired
    private OrderFeignClient orderFeignClient ;

    @Autowired
    private AlipayClient alipayClient ;


    @Override
    public String pay(Long orderId) {

        // 创建一个封装请求参数的对象
        AlipayTradePagePayRequest alipayTradePagePayRequest = new AlipayTradePagePayRequest();
        alipayTradePagePayRequest.setNotifyUrl(aliPayProperties.getNotifyUrl());
        alipayTradePagePayRequest.setReturnUrl(aliPayProperties.getReturnUrl());

        // 构建业务数据
        Result<OrderInfo> infoResult = orderFeignClient.findOrderInfo(orderId);
        OrderInfo orderInfo = infoResult.getData();

        Map<String , Object> params = new HashMap<>() ;
        params.put("out_trade_no" , orderInfo.getOutTradeNo()) ;
        params.put("total_amount" , orderInfo.getTotalAmount()) ;
        params.put("subject" , orderInfo.getTradeBody()) ;
        params.put("product_code" , "FAST_INSTANT_TRADE_PAY") ;

        String toJSONString = JSON.toJSONString(params);
        alipayTradePagePayRequest.setBizContent(toJSONString);

        // 发送请求获取响应结果
        String result = null;
        try {
            result = alipayClient.pageExecute(alipayTradePagePayRequest).getBody();
            log.info("result: {}" , result);
        } catch (AlipayApiException e) {
            throw new RuntimeException(e);
        }
        return result;
    }
}
