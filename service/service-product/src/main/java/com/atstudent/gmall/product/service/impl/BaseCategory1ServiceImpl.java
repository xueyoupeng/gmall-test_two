package com.atstudent.gmall.product.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atstudent.gmall.product.entity.BaseCategory1;
import com.atstudent.gmall.product.service.BaseCategory1Service;
import com.atstudent.gmall.product.mapper.BaseCategory1Mapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author xueyoupeng
* @description 针对表【base_category1(一级分类表)】的数据库操作Service实现
* @createDate 2023-09-05 14:39:24
*/
@Service
public class BaseCategory1ServiceImpl extends ServiceImpl<BaseCategory1Mapper, BaseCategory1>
    implements BaseCategory1Service{

    @Override
    public List<BaseCategory1> findAllBaseCategory1() {
        return this.list();
    }
}




