package com.atstudent.gmall.product.controller;/*
 * @author: XueYouPeng
 * @time: 23.9.6 下午 2:53
 */

import com.atstudent.gmall.common.result.Result;
import com.atstudent.gmall.common.result.ResultCodeEnum;
import com.atstudent.gmall.product.dto.SkuInfoDto;
import com.atstudent.gmall.product.service.SkuInfoService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/admin/product")
public class SkuInfoController {

    @Autowired
    private SkuInfoService skuInfoService;

    /*
     * 获取sku分页列表
     * @author: XueYouPeng
     * @time: 23.9.6 下午 2:53
     */
     @GetMapping(value = "/list/{page}{limit}")
     public Result<Page> findByPage(@PathVariable(value = "page") Integer pageNum,@PathVariable(value = "limit") Integer pageSize){
         Page page = skuInfoService.findByPage(pageNum,pageSize);
         return Result.build(page, ResultCodeEnum.SUCCESS);
     }

     /*
      *
      * @author: XueYouPeng
      * @time: 23.9.6 下午 3:22
      */
     @PostMapping(value = "/saveSkuInfo")
     public Result saveSkuInfo(@RequestBody SkuInfoDto skuInfoDto){
         skuInfoService.saveSkuInfo(skuInfoDto);
         return Result.build(null,ResultCodeEnum.SUCCESS);
     }

     /*
      * 上架
      * @author: XueYouPeng
      * @time: 23.9.6 下午 6:19
      */
    @GetMapping(value = "/onSale/{skuId}")
    public Result onSale(@PathVariable(value = "skuId") Long skuId) {
        skuInfoService.onSale(skuId) ;
        return Result.ok() ;
    }

    /*
     * 下架
     * @author: XueYouPeng
     * @time: 23.9.6 下午 6:19
     */
    @GetMapping(value = "/cancelSale/{skuId}")
    public Result cancelSale(@PathVariable(value = "skuId") Long skuId) {
        skuInfoService.cancelSale(skuId) ;
        return Result.ok() ;
    }
}
