package com.atstudent.gmall.product.controller;/*
 * @author: XueYouPeng
 * @time: 23.9.5 下午 2:49
 */


import com.atstudent.gmall.common.result.Result;
import com.atstudent.gmall.common.result.ResultCodeEnum;
import com.atstudent.gmall.product.entity.BaseCategory2;
import com.atstudent.gmall.product.service.BaseCategory2Service;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/admin/product")
@Api(tags = "二级分类管理")
public class BaseCategory2Controller {

    @Autowired
    private BaseCategory2Service baseCategory2Service;

    /*
     * 根据一级分类查询二级分类
     * @author: XueYouPeng
     * @time: 23.9.5 下午 2:51
     */
    @GetMapping(value = "/getCategory2/{category1Id}")
    @ApiOperation(value = "根据一级分类id查询二级分类")
     public Result<List<BaseCategory2>> findAll(@ApiParam(value = "一级分类的id") @PathVariable(value = "category1Id") Long category1Id){

         List<BaseCategory2> baseCategory2List = baseCategory2Service.findAllCategory2(category1Id);
         return Result.build(baseCategory2List, ResultCodeEnum.SUCCESS);
     }
}
