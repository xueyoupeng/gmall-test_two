package com.atstudent.gmall.product.service;

import com.atstudent.gmall.product.entity.BaseTrademark;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author xueyoupeng
* @description 针对表【base_trademark(品牌表)】的数据库操作Service
* @createDate 2023-09-05 14:39:24
*/
public interface BaseTrademarkService extends IService<BaseTrademark> {

    public abstract Page findByPage(Integer pageNum, Integer pageSize);

    public abstract void deleteById(Long id);
}
