package com.atstudent.gmall.product.controller;/*
 * @author: XueYouPeng
 * @time: 23.9.5 下午 3:13
 */



import com.atstudent.gmall.common.result.Result;
import com.atstudent.gmall.common.result.ResultCodeEnum;
import com.atstudent.gmall.product.entity.BaseTrademark;
import com.atstudent.gmall.product.service.BaseTrademarkService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/admin/product/baseTrademark")
public class BaseTrademarkController {

    @Autowired
    private BaseTrademarkService baseTrademarkService;


    /*
     * 分页查询接口
     * @author: XueYouPeng
     * @time: 23.9.5 下午 3:13
     */
    @GetMapping(value = "{page}/{limit}")
    public Result<Page> findByPage(
            @PathVariable(value = "page") Integer pageNum,
            @PathVariable(value = "limit") Integer pageSize
    ){
        Page page = baseTrademarkService.findByPage(pageNum,pageSize);
        return Result.build(page, ResultCodeEnum.SUCCESS);
    }

    /*
     * 新增品牌
     * @author: XueYouPeng
     * @time: 23.9.5 下午 3:41
     */
     @PostMapping(value = "/save")
     public Result save(@RequestBody BaseTrademark baseTrademark){
         baseTrademarkService.save(baseTrademark);
         return Result.ok();
     }

    /*
     * 修改品牌前的  回显
     * @author: XueYouPeng
     * @time: 23.9.5 下午 4:34
     */
    @GetMapping(value = "get/{id}")
    public Result<BaseTrademark> getById(@PathVariable(value = "id") Long id){
        BaseTrademark baseTrademark = baseTrademarkService.getById(id);
        return Result.build(baseTrademark,ResultCodeEnum.SUCCESS);
    }

    /*
     * 修改品牌接口
     * @author: XueYouPeng
     * @time: 23.9.5 下午 4:35
     */
    @PutMapping(value = "/update")
    public Result update(@RequestBody BaseTrademark baseTrademark){

        baseTrademarkService.updateById(baseTrademark);
        return Result.build(null, ResultCodeEnum.SUCCESS);
    }

    /*
     * 删除品牌接口 根据id
     * @author: XueYouPeng
     * @time: 23.7.11 下午 7:43
     */
    @DeleteMapping(value = "remove/{id}")
    public Result deleteById(@PathVariable(value = "id") Long id){
        baseTrademarkService.deleteById(id);
        return Result.ok();
    }

    /*
     * 获取品牌属性
     * @author: XueYouPeng
     * @time: 23.7.13 上午 10:56
     */
    @GetMapping(value = "/getTrademarkList")
    public Result<List<BaseTrademark>> findAll(){
        List<BaseTrademark> baseTrademarkList = baseTrademarkService.list();
        return Result.build(baseTrademarkList,ResultCodeEnum.SUCCESS);
    }

}
