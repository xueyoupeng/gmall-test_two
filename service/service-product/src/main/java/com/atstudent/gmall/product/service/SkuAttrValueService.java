package com.atstudent.gmall.product.service;

import com.atstudent.gmall.product.entity.SkuAttrValue;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author xueyoupeng
* @description 针对表【sku_attr_value(sku平台属性值关联表)】的数据库操作Service
* @createDate 2023-09-05 14:39:24
*/
public interface SkuAttrValueService extends IService<SkuAttrValue> {

}
