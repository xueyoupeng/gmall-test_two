package com.atstudent.gmall.product.service;

import com.atstudent.gmall.product.entity.SkuImage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author xueyoupeng
* @description 针对表【sku_image(库存单元图片表)】的数据库操作Service
* @createDate 2023-09-05 14:39:24
*/
public interface SkuImageService extends IService<SkuImage> {

}
