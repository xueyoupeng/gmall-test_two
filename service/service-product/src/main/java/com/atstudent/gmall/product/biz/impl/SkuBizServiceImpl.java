package com.atstudent.gmall.product.biz.impl;/*
 * @author: XueYouPeng
 * @time: 23.9.7 下午 1:56
 */

import com.alibaba.fastjson.JSON;
import com.atstudent.gmall.common.constant.GmallConstant;
import com.atstudent.gmall.product.biz.SkuBizService;
import com.atstudent.gmall.product.entity.SkuInfo;
import com.atstudent.gmall.product.entity.SpuSaleAttr;
import com.atstudent.gmall.product.mapper.BaseCategory1Mapper;
import com.atstudent.gmall.product.mapper.SkuInfoMapper;
import com.atstudent.gmall.product.mapper.SkuSaleAttrValueMapper;
import com.atstudent.gmall.product.mapper.SpuSaleAttrMapper;
import com.atstudent.gmall.product.vo.AttrValueConcatVo;
import com.atstudent.gmall.product.vo.CategoryView;
import com.atstudent.gmall.product.vo.SkuDetailVo;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RBloomFilter;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Service
@Slf4j
public class SkuBizServiceImpl implements SkuBizService {

    @Autowired
    private BaseCategory1Mapper baseCategory1Mapper;

    @Autowired
    private SkuInfoMapper skuInfoMapper ;

    @Autowired
    private SpuSaleAttrMapper spuSaleAttrMapper ;

    @Autowired
    private SkuSaleAttrValueMapper skuSaleAttrValueMapper ;

    @Autowired
    private RedissonClient redissonClient;

    @Override
    public CategoryView findCategoryViewBySkuId(Long skuId) {
        return baseCategory1Mapper.findCategoryViewBySkuId(skuId);
    }

    @Override
    public SkuInfo findSkuInfoAndImageBySkuId(Long skuId) {
        return skuInfoMapper.findSkuInfoAndImageBySkuId(skuId);
    }

    @Override
    public SkuInfo findSkuInfoBySkuId(Long skuId) {
        return skuInfoMapper.selectById(skuId);
    }

    @Override
    public List<SpuSaleAttr> findSpuSalAttrBySkuId(Long skuId) {
        return spuSaleAttrMapper.findSpuSalAttrBySkuId(skuId);
    }

    @Override
    public List<AttrValueConcatVo> findSkuAttrValueConcatBySkuId(Long skuId) {
        return skuSaleAttrValueMapper.findSkuAttrValueConcatBySkuId(skuId);
    }

    @Override
    public SkuDetailVo findSkuDetailVo(Long skuId) {

        //创建一个响应对象
        SkuDetailVo skuDetailVo = new SkuDetailVo();
        SkuInfo skuInfoAndImage = findSkuInfoAndImageBySkuId(skuId);
        if (skuInfoAndImage == null){
            return null;
        }
        skuDetailVo.setSkuInfo(skuInfoAndImage);

        CompletableFuture<Void> cf1 = CompletableFuture.runAsync(() -> {
            CategoryView categoryView = findCategoryViewBySkuId(skuId);
            skuDetailVo.setCategoryView(categoryView);
        });

        CompletableFuture<Void> cf2 = CompletableFuture.runAsync(() -> {
            SkuInfo skuInfo = findSkuInfoBySkuId(skuId);
            skuDetailVo.setPrice(skuInfo.getPrice());
        });

        CompletableFuture<Void> cf3 = CompletableFuture.runAsync(() -> {
            List<SpuSaleAttr> spuSaleAttrList = findSpuSalAttrBySkuId(skuId) ;
            skuDetailVo.setSpuSaleAttrList(spuSaleAttrList);
        });

        CompletableFuture<Void> cf4 = CompletableFuture.runAsync(() -> {
            List<AttrValueConcatVo> attrValueConcatVos = findSkuAttrValueConcatBySkuId(skuId);
            Map<Long, String> map = attrValueConcatVos.stream().collect(Collectors.toMap(attrValueConcatVo -> attrValueConcatVo.getSkuId(), attrValueConcatVo -> attrValueConcatVo.getAttrValueConcat()));
            String toJSONString = JSON.toJSONString(map);
            skuDetailVo.setValuesSkuJson(toJSONString);
        });

        CompletableFuture.allOf(cf1,cf2,cf3,cf4).join();

        return skuDetailVo;
    }

    @Override
    public List<Long> findAllSkuIds() {
        List<SkuInfo> skuInfoList = skuInfoMapper.selectList(null);
        List<Long> idList = skuInfoList.stream().map(skuInfo -> skuInfo.getId()).collect(Collectors.toList());
        return idList;
    }

    @PostConstruct
    public void initBloomFilter(){

        //创建一个布隆过滤器
        RBloomFilter<Long> rBloomFilter = redissonClient.getBloomFilter(GmallConstant.REDIS_SKUID_BLOOM_FILTER);
        //初始化
        rBloomFilter.tryInit(1000000 , 0.000001);
        //缓存商品id
        List<Long> allSkuIds = findAllSkuIds();
        allSkuIds.stream().forEach(skuId -> rBloomFilter.add(skuId));
        log.info("分布式的布隆过滤器初始化成功了...");
    }
}
