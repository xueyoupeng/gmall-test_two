package com.atstudent.gmall.product.controller;/*
 * @author: XueYouPeng
 * @time: 23.9.4 下午 8:46
 */




import com.atstudent.gmall.common.result.Result;
import com.atstudent.gmall.common.result.ResultCodeEnum;
import com.atstudent.gmall.product.entity.BaseCategory1;
import com.atstudent.gmall.product.service.BaseCategory1Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/admin/product")
public class BaseCategory1Controller {

    @Autowired
    private BaseCategory1Service baseCategory1Service;


    //查询一级接口
    @GetMapping(value = "/getCategory1")
    public Result<List<BaseCategory1>> find_one(){

        List<BaseCategory1> baseCategory1List = baseCategory1Service.findAllBaseCategory1();

        return Result.build(baseCategory1List, ResultCodeEnum.SUCCESS);
    }
}
