package com.atstudent.gmall.product.service;

import com.atstudent.gmall.product.dto.SpuInfoDto;
import com.atstudent.gmall.product.entity.SpuInfo;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author xueyoupeng
* @description 针对表【spu_info(商品表)】的数据库操作Service
* @createDate 2023-09-05 14:39:24
*/
public interface SpuInfoService extends IService<SpuInfo> {

    public abstract Page findByPage(Integer pageNum, Integer pageSize, Long category3Id);

    public abstract void saveSpuInfo(SpuInfoDto spuInfoDto);
}
