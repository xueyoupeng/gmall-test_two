package com.atstudent.gmall.product.service;

import com.atstudent.gmall.product.entity.BaseCategory2;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author xueyoupeng
* @description 针对表【base_category2(二级分类表)】的数据库操作Service
* @createDate 2023-09-05 14:39:24
*/
public interface BaseCategory2Service extends IService<BaseCategory2> {

    public abstract List<BaseCategory2> findAllCategory2(Long category1Id);
}
