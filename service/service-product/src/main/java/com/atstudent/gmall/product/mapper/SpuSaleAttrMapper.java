package com.atstudent.gmall.product.mapper;

import com.atstudent.gmall.product.entity.SpuSaleAttr;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
* @author xueyoupeng
* @description 针对表【spu_sale_attr(spu销售属性)】的数据库操作Mapper
* @createDate 2023-09-05 14:39:24
* @Entity com.atstudent.gmall.product.entity.SpuSaleAttr
*/
public interface SpuSaleAttrMapper extends BaseMapper<SpuSaleAttr> {

    public abstract List<SpuSaleAttr> findBySpuId(Long spuId);

    public abstract List<SpuSaleAttr> findSpuSalAttrBySkuId(Long skuId);
}




