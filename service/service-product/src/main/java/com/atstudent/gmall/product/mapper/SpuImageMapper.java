package com.atstudent.gmall.product.mapper;

import com.atstudent.gmall.product.entity.SpuImage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author xueyoupeng
* @description 针对表【spu_image(商品图片表)】的数据库操作Mapper
* @createDate 2023-09-05 14:39:24
* @Entity com.atstudent.gmall.product.entity.SpuImage
*/
public interface SpuImageMapper extends BaseMapper<SpuImage> {

}




