package com.atstudent.gmall.product.controller;/*
 * @author: XueYouPeng
 * @time: 23.9.6 下午 3:27
 */

import com.atstudent.gmall.common.result.Result;
import com.atstudent.gmall.common.result.ResultCodeEnum;
import com.atstudent.gmall.product.entity.SpuSaleAttr;
import com.atstudent.gmall.product.service.SpuSaleAttrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/admin/product")
public class SpuSaleAttrController {

    @Autowired
    private SpuSaleAttrService spuSaleAttrService;

    /*
     * 根据spuId获取销售属性
     * @author: XueYouPeng
     * @time: 23.9.6 下午 3:27
     */
    @GetMapping(value = "/spuSaleAttrList/{spuId}")
    public Result<List<SpuSaleAttr>> findBySpuId(@PathVariable(value = "spuId") Long spuId){
        List<SpuSaleAttr> spuSaleAttrList = spuSaleAttrService.findBySpuId(spuId);
        return Result.build(spuSaleAttrList, ResultCodeEnum.SUCCESS);
    }
}
