package com.atstudent.gmall.product.mapper;

import com.atstudent.gmall.product.entity.BaseTrademark;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author xueyoupeng
* @description 针对表【base_trademark(品牌表)】的数据库操作Mapper
* @createDate 2023-09-05 14:39:24
* @Entity com.atstudent.gmall.product.entity.BaseTrademark
*/
public interface BaseTrademarkMapper extends BaseMapper<BaseTrademark> {

}




