package com.atstudent.gmall.product.service;

import com.atstudent.gmall.product.entity.BaseSaleAttr;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author xueyoupeng
* @description 针对表【base_sale_attr(基本销售属性表)】的数据库操作Service
* @createDate 2023-09-05 14:39:24
*/
public interface BaseSaleAttrService extends IService<BaseSaleAttr> {

}
