package com.atstudent.gmall.product.service;

import com.atstudent.gmall.product.entity.SpuSaleAttr;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author xueyoupeng
* @description 针对表【spu_sale_attr(spu销售属性)】的数据库操作Service
* @createDate 2023-09-05 14:39:24
*/
public interface SpuSaleAttrService extends IService<SpuSaleAttr> {

    public abstract List<SpuSaleAttr> findBySpuId(Long spuId);
}
