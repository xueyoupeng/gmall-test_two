package com.atstudent.gmall.product.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atstudent.gmall.product.entity.SkuSaleAttrValue;
import com.atstudent.gmall.product.service.SkuSaleAttrValueService;
import com.atstudent.gmall.product.mapper.SkuSaleAttrValueMapper;
import org.springframework.stereotype.Service;

/**
* @author xueyoupeng
* @description 针对表【sku_sale_attr_value(sku销售属性值)】的数据库操作Service实现
* @createDate 2023-09-05 14:39:24
*/
@Service
public class SkuSaleAttrValueServiceImpl extends ServiceImpl<SkuSaleAttrValueMapper, SkuSaleAttrValue>
    implements SkuSaleAttrValueService{

}




