package com.atstudent.gmall.product.mapper;

import com.atstudent.gmall.product.entity.SkuSaleAttrValue;
import com.atstudent.gmall.product.vo.AttrValueConcatVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
* @author xueyoupeng
* @description 针对表【sku_sale_attr_value(sku销售属性值)】的数据库操作Mapper
* @createDate 2023-09-05 14:39:24
* @Entity com.atstudent.gmall.product.entity.SkuSaleAttrValue
*/
public interface SkuSaleAttrValueMapper extends BaseMapper<SkuSaleAttrValue> {

    public abstract List<AttrValueConcatVo> findSkuAttrValueConcatBySkuId(Long skuId);
}




