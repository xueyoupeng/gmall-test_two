package com.atstudent.gmall.product.mapper;

import com.atstudent.gmall.product.entity.SkuInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author xueyoupeng
* @description 针对表【sku_info(库存单元表)】的数据库操作Mapper
* @createDate 2023-09-05 14:39:24
* @Entity com.atstudent.gmall.product.entity.SkuInfo
*/
public interface SkuInfoMapper extends BaseMapper<SkuInfo> {

    public abstract SkuInfo findSkuInfoAndImageBySkuId(Long skuId);
}




