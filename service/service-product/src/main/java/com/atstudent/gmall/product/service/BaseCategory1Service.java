package com.atstudent.gmall.product.service;

import com.atstudent.gmall.product.entity.BaseCategory1;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author xueyoupeng
* @description 针对表【base_category1(一级分类表)】的数据库操作Service
* @createDate 2023-09-05 14:39:24
*/
public interface BaseCategory1Service extends IService<BaseCategory1> {


    public abstract List<BaseCategory1> findAllBaseCategory1();
}
