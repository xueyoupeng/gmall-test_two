package com.atstudent.gmall.product.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atstudent.gmall.product.entity.SpuPoster;
import com.atstudent.gmall.product.service.SpuPosterService;
import com.atstudent.gmall.product.mapper.SpuPosterMapper;
import org.springframework.stereotype.Service;

/**
* @author xueyoupeng
* @description 针对表【spu_poster(商品海报表)】的数据库操作Service实现
* @createDate 2023-09-05 14:39:24
*/
@Service
public class SpuPosterServiceImpl extends ServiceImpl<SpuPosterMapper, SpuPoster>
    implements SpuPosterService{

}




