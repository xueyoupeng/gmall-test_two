package com.atstudent.gmall.product.controller;/*
 * @author: XueYouPeng
 * @time: 23.9.6 上午 11:24
 */

import com.atstudent.gmall.common.result.Result;
import com.atstudent.gmall.common.result.ResultCodeEnum;
import com.atstudent.gmall.product.entity.BaseSaleAttr;
import com.atstudent.gmall.product.service.BaseSaleAttrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/admin/product")
public class BaseSaleAttrController {

    @Autowired
    private BaseSaleAttrService baseSaleAttrService;

    /*
     * 获取销售属性
     * @author: XueYouPeng
     * @time: 23.9.6 上午 11:25
     */
    @GetMapping(value = "/baseSaleAttrList")
    public Result<List<BaseSaleAttr>> findAll(){
        List<BaseSaleAttr> baseSaleAttrList = baseSaleAttrService.list();
        return Result.build(baseSaleAttrList, ResultCodeEnum.SUCCESS);
    }
}
