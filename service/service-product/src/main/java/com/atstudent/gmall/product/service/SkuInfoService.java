package com.atstudent.gmall.product.service;

import com.atstudent.gmall.product.dto.SkuInfoDto;
import com.atstudent.gmall.product.entity.SkuInfo;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author xueyoupeng
* @description 针对表【sku_info(库存单元表)】的数据库操作Service
* @createDate 2023-09-05 14:39:24
*/
public interface SkuInfoService extends IService<SkuInfo> {

    public abstract Page findByPage(Integer pageNum, Integer pageSize);

    public abstract void saveSkuInfo(SkuInfoDto skuInfoDto);

    public abstract void onSale(Long skuId);

    public abstract void cancelSale(Long skuId);
}
