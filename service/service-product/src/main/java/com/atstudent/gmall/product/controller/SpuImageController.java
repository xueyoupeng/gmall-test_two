package com.atstudent.gmall.product.controller;/*
 * @author: XueYouPeng
 * @time: 23.9.6 下午 3:19
 */

import com.atstudent.gmall.common.result.Result;
import com.atstudent.gmall.common.result.ResultCodeEnum;
import com.atstudent.gmall.product.entity.SpuImage;
import com.atstudent.gmall.product.service.SpuImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/admin/product")
public class SpuImageController {

    @Autowired
    private SpuImageService spuImageService;

    /*
     * 根据spuId获取图片列表
     * @author: XueYouPeng
     * @time: 23.9.6 下午 3:19
     */
    @GetMapping(value = "/spuImageList/{spuId}")
    public Result<List<SpuImage>> findBySpuId(@PathVariable(value = "spuId") Long spuId){
        List<SpuImage> spuImageList = spuImageService.findBySpuId(spuId);
        return Result.build(spuImageList, ResultCodeEnum.SUCCESS);
    }
}
