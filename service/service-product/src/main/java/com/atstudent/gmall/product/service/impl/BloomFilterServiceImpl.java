package com.atstudent.gmall.product.service.impl;/*
 * @author: XueYouPeng
 * @time: 23.9.10 下午 4:55
 */

import com.atstudent.gmall.common.constant.GmallConstant;
import com.atstudent.gmall.product.biz.SkuBizService;
import com.atstudent.gmall.product.service.BloomFilterService;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RBloomFilter;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
@Slf4j
public class BloomFilterServiceImpl implements BloomFilterService {

    @Autowired
    private RedissonClient redissonClient;

    @Autowired
    private SkuBizService skuBizService;

    @Autowired
    private RedisTemplate<String , String> redisTemplate;

    @Override
    public void resetBloomFilter() {

        //创建一个新的布隆过滤器
        RBloomFilter<Object> newBloomFilter = redissonClient.getBloomFilter(GmallConstant.REDIS_SKUID_BLOOM_FILTER_NEW);
        //初始化
        newBloomFilter.tryInit(1000000 , 0.000001);
        //执行 添加数据的 业务代码
        List<Long> allSkuIds = skuBizService.findAllSkuIds();
        allSkuIds.forEach(skuId -> newBloomFilter.add(skuId));
        log.info("新的布隆过滤器初始化好了...");

        //删除之前的布隆过滤器 对新的进行重命名 跟之前的保持一致
        //用lua脚本
        String script = "redis.call('del' , KEYS[1])\n" +
                "redis.call('del' , \"{\" .. KEYS[1] .. \"}:config\")\n" +
                "redis.call('rename' , KEYS[2] , KEYS[1])\n" +
                "redis.call('rename' , \"{\" .. KEYS[2] .. \"}:config\" , \"{\" .. KEYS[1] .. \"}:config\")\n" +
                "return 1";

        //执行lua脚本
        Long result = redisTemplate.execute(new DefaultRedisScript<>(script, Long.class), Arrays.asList(GmallConstant.REDIS_SKUID_BLOOM_FILTER, GmallConstant.REDIS_SKUID_BLOOM_FILTER_NEW));
        if (result == 1){
            log.info("布隆过滤器重置成功了...");
        }

    }
}
