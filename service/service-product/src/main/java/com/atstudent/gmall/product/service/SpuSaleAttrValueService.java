package com.atstudent.gmall.product.service;

import com.atstudent.gmall.product.entity.SpuSaleAttrValue;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author xueyoupeng
* @description 针对表【spu_sale_attr_value(spu销售属性值)】的数据库操作Service
* @createDate 2023-09-05 14:39:24
*/
public interface SpuSaleAttrValueService extends IService<SpuSaleAttrValue> {

}
