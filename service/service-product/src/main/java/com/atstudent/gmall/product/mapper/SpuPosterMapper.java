package com.atstudent.gmall.product.mapper;

import com.atstudent.gmall.product.entity.SpuPoster;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author xueyoupeng
* @description 针对表【spu_poster(商品海报表)】的数据库操作Mapper
* @createDate 2023-09-05 14:39:24
* @Entity com.atstudent.gmall.product.entity.SpuPoster
*/
public interface SpuPosterMapper extends BaseMapper<SpuPoster> {

}




