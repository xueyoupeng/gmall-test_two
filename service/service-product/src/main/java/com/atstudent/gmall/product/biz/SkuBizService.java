package com.atstudent.gmall.product.biz;/*
 * @author: XueYouPeng
 * @time: 23.9.7 下午 1:55
 */

import com.atstudent.gmall.product.entity.SkuInfo;
import com.atstudent.gmall.product.entity.SpuSaleAttr;
import com.atstudent.gmall.product.vo.AttrValueConcatVo;
import com.atstudent.gmall.product.vo.CategoryView;
import com.atstudent.gmall.product.vo.SkuDetailVo;

import java.util.List;

public interface SkuBizService {
    public abstract CategoryView findCategoryViewBySkuId(Long skuId);

    public abstract SkuInfo findSkuInfoAndImageBySkuId(Long skuId);

    public abstract SkuInfo findSkuInfoBySkuId(Long skuId);

    public abstract List<SpuSaleAttr> findSpuSalAttrBySkuId(Long skuId);

    public abstract List<AttrValueConcatVo> findSkuAttrValueConcatBySkuId(Long skuId);

    public abstract SkuDetailVo findSkuDetailVo(Long skuId);

    public abstract List<Long> findAllSkuIds();
}
