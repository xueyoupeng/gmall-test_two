package com.atstudent.gmall.product.service;

import com.atstudent.gmall.product.entity.BaseCategory3;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author xueyoupeng
* @description 针对表【base_category3(三级分类表)】的数据库操作Service
* @createDate 2023-09-05 14:39:24
*/
public interface BaseCategory3Service extends IService<BaseCategory3> {

    public abstract List<BaseCategory3> findBaseCategory3ByC2Id(Long category2Id);
}
