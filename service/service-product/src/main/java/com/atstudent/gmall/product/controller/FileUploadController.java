package com.atstudent.gmall.product.controller;/*
 * @author: XueYouPeng
 * @time: 23.9.5 下午 5:40
 */

import com.atstudent.gmall.common.result.Result;
import com.atstudent.gmall.common.result.ResultCodeEnum;
import com.atstudent.gmall.common.service.FileUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping(value = "/admin/product")
public class FileUploadController {

    @Autowired
    private FileUploadService fileUploadService;

    /*
     * 品牌列表新增时 图片文件上传
     * @author: XueYouPeng
     * @time: 23.9.5 下午 6:29
     */
     @PostMapping(value = "/fileUpload")
     public Result<String> upload(
             @RequestParam(value = "file") MultipartFile multipartFile
     ){
         String url = fileUploadService.upload(multipartFile);
         return Result.build(url, ResultCodeEnum.SUCCESS);
     }
}
