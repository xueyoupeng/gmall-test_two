package com.atstudent.gmall.product.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atstudent.gmall.product.entity.SpuSaleAttrValue;
import com.atstudent.gmall.product.service.SpuSaleAttrValueService;
import com.atstudent.gmall.product.mapper.SpuSaleAttrValueMapper;
import org.springframework.stereotype.Service;

/**
* @author xueyoupeng
* @description 针对表【spu_sale_attr_value(spu销售属性值)】的数据库操作Service实现
* @createDate 2023-09-05 14:39:24
*/
@Service
public class SpuSaleAttrValueServiceImpl extends ServiceImpl<SpuSaleAttrValueMapper, SpuSaleAttrValue>
    implements SpuSaleAttrValueService{

}




