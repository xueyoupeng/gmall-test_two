package com.atstudent.gmall.product.service;

import com.atstudent.gmall.product.entity.BaseAttrValue;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author xueyoupeng
* @description 针对表【base_attr_value(属性值表)】的数据库操作Service
* @createDate 2023-09-05 14:39:24
*/
public interface BaseAttrValueService extends IService<BaseAttrValue> {

    public abstract List<BaseAttrValue> findByAttrId(Long attrId);
}
