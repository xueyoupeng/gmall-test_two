package com.atstudent.gmall.product.mapper;

import com.atstudent.gmall.product.entity.BaseAttrInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author xueyoupeng
* @description 针对表【base_attr_info(属性表)】的数据库操作Mapper
* @createDate 2023-09-05 14:39:24
* @Entity com.atstudent.gmall.product.entity.BaseAttrInfo
*/
public interface BaseAttrInfoMapper extends BaseMapper<BaseAttrInfo> {

    List<BaseAttrInfo> findBaseAttrInfo(@Param(value = "c1Id") Long c1Id, @Param(value = "c2Id")Long c2Id, @Param(value = "c3Id")Long c3Id);
}




