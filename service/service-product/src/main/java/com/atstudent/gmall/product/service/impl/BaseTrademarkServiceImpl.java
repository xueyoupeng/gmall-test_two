package com.atstudent.gmall.product.service.impl;

import com.atstudent.gmall.common.execption.GmallException;
import com.atstudent.gmall.common.result.ResultCodeEnum;
import com.atstudent.gmall.product.entity.SkuInfo;
import com.atstudent.gmall.product.entity.SpuInfo;
import com.atstudent.gmall.product.mapper.SkuInfoMapper;
import com.atstudent.gmall.product.mapper.SpuInfoMapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atstudent.gmall.product.entity.BaseTrademark;
import com.atstudent.gmall.product.service.BaseTrademarkService;
import com.atstudent.gmall.product.mapper.BaseTrademarkMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author xueyoupeng
* @description 针对表【base_trademark(品牌表)】的数据库操作Service实现
* @createDate 2023-09-05 14:39:24
*/
@Service
public class BaseTrademarkServiceImpl extends ServiceImpl<BaseTrademarkMapper, BaseTrademark>
    implements BaseTrademarkService{

    @Autowired
    private SkuInfoMapper skuInfoMapper;

    @Autowired
    private SpuInfoMapper spuInfoMapper;

    @Override
    public Page findByPage(Integer pageNum, Integer pageSize) {

        Page page = new Page(pageNum, pageSize);
        this.page(page);
        return page;
    }

    @Override
    public void deleteById(Long id) {
        // 根据品牌的id查询spu
        QueryWrapper<SpuInfo> spuInfoQueryWrapper = new QueryWrapper<>();
        spuInfoQueryWrapper.eq("tm_id",id);
        List<SpuInfo> spuInfoList = spuInfoMapper.selectList(spuInfoQueryWrapper);
        if (spuInfoList != null && spuInfoList.size() > 0){
            throw new GmallException(ResultCodeEnum.ERROR_SPU_REF);
        }
        // 根据品牌的id查询sku
        LambdaQueryWrapper<SkuInfo> skuInfoLambdaQueryWrapper = new LambdaQueryWrapper<>();
        skuInfoLambdaQueryWrapper.eq(SkuInfo::getTmId,id);
        List<SkuInfo> skuInfoList = skuInfoMapper.selectList(skuInfoLambdaQueryWrapper);
        if (skuInfoList != null && skuInfoList.size() > 0){
            throw new GmallException(ResultCodeEnum.ERROR_SKU_REF);
        }
        // 根据品牌的id删除品牌
        this.removeById(id);
    }
}




