package com.atstudent.gmall.product.biz;/*
 * @author: XueYouPeng
 * @time: 23.9.7 上午 8:35
 */

import com.atstudent.gmall.product.vo.CategoryVo;

import java.util.List;

public interface BaseCategoryBizService {
    public abstract List<CategoryVo> findAllCategoryTree();
}
