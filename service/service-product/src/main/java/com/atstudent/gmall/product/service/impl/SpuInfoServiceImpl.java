package com.atstudent.gmall.product.service.impl;

import com.atstudent.gmall.product.dto.SpuInfoDto;
import com.atstudent.gmall.product.entity.SpuImage;
import com.atstudent.gmall.product.entity.SpuSaleAttr;
import com.atstudent.gmall.product.entity.SpuSaleAttrValue;
import com.atstudent.gmall.product.service.SpuImageService;
import com.atstudent.gmall.product.service.SpuSaleAttrService;
import com.atstudent.gmall.product.service.SpuSaleAttrValueService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atstudent.gmall.product.entity.SpuInfo;
import com.atstudent.gmall.product.service.SpuInfoService;
import com.atstudent.gmall.product.mapper.SpuInfoMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
* @author xueyoupeng
* @description 针对表【spu_info(商品表)】的数据库操作Service实现
* @createDate 2023-09-05 14:39:24
*/
@Service
public class SpuInfoServiceImpl extends ServiceImpl<SpuInfoMapper, SpuInfo>
    implements SpuInfoService{

    @Autowired
    private SpuImageService spuImageService;

    @Autowired
    private SpuSaleAttrService spuSaleAttrService;

    @Autowired
    private SpuSaleAttrValueService spuSaleAttrValueService;

    @Override
    public Page findByPage(Integer pageNum, Integer pageSize, Long category3Id) {
        //带条件的分页
        Page page = new Page(pageNum,pageSize);
        //构建条件
        LambdaQueryWrapper<SpuInfo> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(SpuInfo::getCategory3Id,category3Id);

        this.page(page,lambdaQueryWrapper);

        return page;
    }

    @Override
    public void saveSpuInfo(SpuInfoDto spuInfoDto) {

        // 保存spu的基本数据
        SpuInfo spuInfo = new SpuInfo();
        BeanUtils.copyProperties(spuInfoDto,spuInfo);
        this.save(spuInfo);

        // 保存spu的图片数据
        List<SpuImage> spuImageList = spuInfoDto.getSpuImageList();
        //补全缺少的字段
        spuImageList.stream().map(spuImage -> {
            spuImage.setSpuId(spuInfo.getId());
            return spuImage;
        }).collect(Collectors.toList());
        spuImageService.saveBatch(spuImageList);

        // 保存spu的销售属性名
        List<SpuSaleAttr> spuSaleAttrList = spuInfoDto.getSpuSaleAttrList();
        //补全缺少的字段
        spuSaleAttrList.stream().map(spuSaleAttr -> {
            spuSaleAttr.setSpuId(spuInfo.getId());
            return spuSaleAttr;
        }).collect(Collectors.toList());
        spuSaleAttrService.saveBatch(spuSaleAttrList);

        // 保存spu的销售属性值
        spuSaleAttrList.stream().forEach(spuSaleAttr -> {
            List<SpuSaleAttrValue> spuSaleAttrValueList = spuSaleAttr.getSpuSaleAttrValueList();
            spuSaleAttrValueList.stream().map(spuSaleAttrValue -> {
                spuSaleAttrValue.setSaleAttrName(spuSaleAttr.getSaleAttrName());
                spuSaleAttrValue.setSpuId(spuInfo.getId());
                return spuSaleAttrValue;
            }).collect(Collectors.toList());
            spuSaleAttrValueService.saveBatch(spuSaleAttrValueList);
        });
    }
}




