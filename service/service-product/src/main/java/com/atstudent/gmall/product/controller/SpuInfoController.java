package com.atstudent.gmall.product.controller;/*
 * @author: XueYouPeng
 * @time: 23.9.6 上午 10:30
 */

import com.atstudent.gmall.common.result.Result;
import com.atstudent.gmall.common.result.ResultCodeEnum;
import com.atstudent.gmall.product.dto.SpuInfoDto;
import com.atstudent.gmall.product.service.SpuInfoService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/admin/product")
public class SpuInfoController {

    @Autowired
    private SpuInfoService spuInfoService;

    /*
     * 获取spu分页列表
     * @author: XueYouPeng
     * @time: 23.9.6 上午 10:36
     */
    @GetMapping(value = "/{page}/{limit}")
    public Result<Page> findByPage(
            @PathVariable(value = "page") Integer pageNum,
            @PathVariable(value = "limit") Integer pageSize,
            @RequestParam(value = "category3Id") Long category3Id
    ){
        Page page = spuInfoService.findByPage(pageNum,pageSize,category3Id);
        return Result.build(page, ResultCodeEnum.SUCCESS);
    }

    /*
     * 添加spu
     * @author: XueYouPeng
     * @time: 23.9.6 下午 2:20
     */
    @PostMapping(value = "/saveSpuInfo")
    public Result saveSpuInfo(@RequestBody SpuInfoDto spuInfoDto){
        spuInfoService.saveSpuInfo(spuInfoDto);
        return Result.build(null,ResultCodeEnum.SUCCESS);
    }

}
