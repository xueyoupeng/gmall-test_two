package com.atstudent.gmall.product.controller;/*
 * @author: XueYouPeng
 * @time: 23.9.5 下午 9:14
 */

import com.atstudent.gmall.common.result.Result;
import com.atstudent.gmall.common.result.ResultCodeEnum;
import com.atstudent.gmall.product.entity.BaseAttrInfo;
import com.atstudent.gmall.product.service.BaseAttrInfoService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/admin/product")
@Api(tags = "平台属性管理")
public class BaseAttrInfoController {

    @Autowired
    private BaseAttrInfoService baseAttrInfoService;

    /*
     * 根据分类id获取平台属性
     * @author: XueYouPeng
     * @time: 23.9.5 下午 9:14
     */
    @GetMapping(value = "/attrInfoList/{category1Id}/{category2Id}/{category3Id}")
    public Result<List<BaseAttrInfo>> findBaseAttrInfo(
            @PathVariable(value = "category1Id") Long c1Id,
            @PathVariable(value = "category2Id") Long c2Id,
            @PathVariable(value = "category3Id") Long c3Id
    ){
        List<BaseAttrInfo> baseAttrInfoList = baseAttrInfoService.findBaseAttrInfo(c1Id,c2Id,c3Id);
        return Result.build(baseAttrInfoList, ResultCodeEnum.SUCCESS);

    }

    /*
     * 添加平台属性
     * @author: XueYouPeng
     * @time: 23.9.5 下午 9:44
     */
     @PostMapping(value = "/saveAttrInfo")
     public Result saveBaseAttrInfo(@RequestBody BaseAttrInfo baseAttrInfo){
         baseAttrInfoService.saveBaseAttrInfo(baseAttrInfo);
         return Result.build(null,ResultCodeEnum.SUCCESS);

     }
}
