package com.atstudent.gmall.product.mapper;

import com.atstudent.gmall.product.entity.SpuInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author xueyoupeng
* @description 针对表【spu_info(商品表)】的数据库操作Mapper
* @createDate 2023-09-05 14:39:24
* @Entity com.atstudent.gmall.product.entity.SpuInfo
*/
public interface SpuInfoMapper extends BaseMapper<SpuInfo> {

}




