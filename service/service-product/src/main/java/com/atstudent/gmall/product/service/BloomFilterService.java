package com.atstudent.gmall.product.service;/*
 * @author: XueYouPeng
 * @time: 23.9.10 下午 4:55
 */

public interface BloomFilterService {

    public abstract void resetBloomFilter();
}
