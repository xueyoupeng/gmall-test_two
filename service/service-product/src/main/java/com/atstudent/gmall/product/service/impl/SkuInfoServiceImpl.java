package com.atstudent.gmall.product.service.impl;

import com.atstudent.gmall.common.feign.search.SearchFeignClient;
import com.atstudent.gmall.product.biz.SkuBizService;
import com.atstudent.gmall.product.dto.SkuInfoDto;
import com.atstudent.gmall.product.entity.*;
import com.atstudent.gmall.product.mapper.SkuAttrValueMapper;
import com.atstudent.gmall.product.service.*;
import com.atstudent.gmall.product.vo.CategoryView;
import com.atstudent.gmall.search.entity.Goods;
import com.atstudent.gmall.search.entity.SearchAttr;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atstudent.gmall.product.mapper.SkuInfoMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

/**
* @author xueyoupeng
* @description 针对表【sku_info(库存单元表)】的数据库操作Service实现
* @createDate 2023-09-05 14:39:24
*/
@Service
public class SkuInfoServiceImpl extends ServiceImpl<SkuInfoMapper, SkuInfo>
    implements SkuInfoService{

    @Autowired
    private SkuImageService skuImageService;

    @Autowired
    private SkuAttrValueService skuAttrValueService;

    @Autowired
    private SkuSaleAttrValueService skuSaleAttrValueService;

    @Autowired
    private SearchFeignClient searchFeignClient;

    @Autowired
    private BaseTrademarkService baseTrademarkService ;

    @Autowired
    private SkuBizService skuBizService;

    @Autowired
    private SkuAttrValueMapper skuAttrValueMapper;

    @Override
    public Page findByPage(Integer pageNum, Integer pageSize) {

        Page page = new Page(pageNum,pageSize);
        this.page(page);
        return page;
    }

    @Override
    @Transactional
    public void saveSkuInfo(SkuInfoDto skuInfoDto) {

        //保存sku的基本信息
        SkuInfo skuInfo = new SkuInfo();
        BeanUtils.copyProperties(skuInfoDto,skuInfo);
        //单独设置 <是否销售> 字段
        skuInfo.setIsSale(0);
        this.save(skuInfo);


        //保存sku的图片数据
        List<SkuImage> skuImageList = skuInfoDto.getSkuImageList();
        //缺少sku_id字段
        skuImageList.stream().map(skuImage -> {
            skuImage.setSkuId(skuInfo.getId());
            return skuImage;
        }).collect(Collectors.toList());
        skuImageService.saveBatch(skuImageList);


        //保存sku的平台属性值数据
        List<SkuAttrValue> skuAttrValueList = skuInfoDto.getSkuAttrValueList();
        //缺少sku_id字段
        skuAttrValueList.stream().map(skuAttrValue -> {
            skuAttrValue.setSkuId(skuInfo.getId());
            return skuAttrValue;
        }).collect(Collectors.toList());
        skuAttrValueService.saveBatch(skuAttrValueList);

        //保存sku的销售属性值数据
        List<SkuSaleAttrValue> skuSaleAttrValueList = skuInfoDto.getSkuSaleAttrValueList();
        //缺少sku_id字段 和 spu_id字段
        skuSaleAttrValueList = skuSaleAttrValueList.stream().map(skuSaleAttrValue -> {
            skuSaleAttrValue.setSpuId(skuInfoDto.getSpuId());//自定义的SkuInfoDto里面就有spu_id
            skuSaleAttrValue.setSkuId(skuInfo.getId());
            return skuSaleAttrValue;
        }).collect(Collectors.toList());
        skuSaleAttrValueService.saveBatch(skuSaleAttrValueList);
    }

    @Override
    public void onSale(Long skuId) {

        //先查到这个商品
        SkuInfo skuInfo = this.getById(skuId);
        skuInfo.setIsSale(1);
        this.updateById(skuInfo);

        //远程调用service-search微服务的接口，把商品数据添加到es索引库中
        Goods goods = buildGoods(skuId);
        searchFeignClient.saveGoods(goods);
    }

    private Goods buildGoods(Long skuId) {

        // 创建Goods对象
        Goods goods = new Goods();
        goods.setId(skuId);

        // 设置sku的基本数据
        //根据skuId查询商品数据 不用异步编排 因为下面有查询需要用到skuInfo
        SkuInfo skuInfo = this.getById(skuId);
        goods.setDefaultImg(skuInfo.getSkuDefaultImg());
        goods.setTitle(skuInfo.getSkuName());
        goods.setPrice(skuInfo.getPrice());
        goods.setCreateTime(new Date());



        // 设置品牌数据
        CompletableFuture<Void> cf1 = CompletableFuture.runAsync(() -> {
            BaseTrademark baseTrademark = baseTrademarkService.getById(skuInfo.getTmId());
            goods.setTmId(baseTrademark.getId());
            goods.setTmName(baseTrademark.getTmName());
            goods.setTmLogoUrl(baseTrademark.getLogoUrl());
        });

        // 根据skuId查询三级分类数据
        CompletableFuture<Void> cf2 = CompletableFuture.runAsync(() -> {
            CategoryView categoryView = skuBizService.findCategoryViewBySkuId(skuId);
            goods.setCategory1Id(categoryView.getCategory1Id());
            goods.setCategory1Name(categoryView.getCategory1Name());
            goods.setCategory2Id(categoryView.getCategory2Id());
            goods.setCategory2Name(categoryView.getCategory2Name());
            goods.setCategory3Id(categoryView.getCategory3Id());
            goods.setCategory3Name(categoryView.getCategory3Name());
        });

        // 设置热度分
        goods.setHotScore(0L);

        // 平台属性和平台属性值
        CompletableFuture<Void> cf3 = CompletableFuture.runAsync(() -> {
            List<SearchAttr> searchAttrList = skuAttrValueMapper.findSearchAttrBySkuId(skuId);
            goods.setAttrs(searchAttrList);
        });

        CompletableFuture.allOf(cf1 , cf2 , cf3).join();

        return goods;

    }

    @Override
    public void cancelSale(Long skuId) {

        SkuInfo skuInfo = this.getById(skuId);
        skuInfo.setIsSale(0);
        this.updateById(skuInfo) ;

        // 远程调用service-search微服务的接口，从ES索引库中删除商品数据
        searchFeignClient.deleteById(skuId);
    }
}




