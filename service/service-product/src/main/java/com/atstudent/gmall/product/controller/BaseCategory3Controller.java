package com.atstudent.gmall.product.controller;/*
 * @author: XueYouPeng
 * @time: 23.9.5 下午 3:00
 */

import com.atstudent.gmall.common.result.Result;
import com.atstudent.gmall.common.result.ResultCodeEnum;
import com.atstudent.gmall.product.entity.BaseCategory3;
import com.atstudent.gmall.product.service.BaseCategory3Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/admin/product")
public class BaseCategory3Controller {

    @Autowired
    private BaseCategory3Service baseCategory3Service ;


    /*
     * 根据二级分类查询三级分类
     * @author: XueYouPeng
     * @time: 23.9.5 下午 3:01
     */
    @GetMapping(value = "/getCategory3/{category2Id}")
    public Result<List<BaseCategory3>> findBaseCategory3ByC2Id(@PathVariable(value = "category2Id") Long category2Id){
        List<BaseCategory3> baseCategory3List = baseCategory3Service.findBaseCategory3ByC2Id(category2Id);
        return Result.build(baseCategory3List, ResultCodeEnum.SUCCESS);
    }
}
