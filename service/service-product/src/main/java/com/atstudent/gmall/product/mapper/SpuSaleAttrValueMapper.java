package com.atstudent.gmall.product.mapper;

import com.atstudent.gmall.product.entity.SpuSaleAttrValue;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author xueyoupeng
* @description 针对表【spu_sale_attr_value(spu销售属性值)】的数据库操作Mapper
* @createDate 2023-09-05 14:39:24
* @Entity com.atstudent.gmall.product.entity.SpuSaleAttrValue
*/
public interface SpuSaleAttrValueMapper extends BaseMapper<SpuSaleAttrValue> {

}




