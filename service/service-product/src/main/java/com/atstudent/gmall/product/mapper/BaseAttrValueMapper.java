package com.atstudent.gmall.product.mapper;

import com.atstudent.gmall.product.entity.BaseAttrValue;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author xueyoupeng
* @description 针对表【base_attr_value(属性值表)】的数据库操作Mapper
* @createDate 2023-09-05 14:39:24
* @Entity com.atstudent.gmall.product.entity.BaseAttrValue
*/
public interface BaseAttrValueMapper extends BaseMapper<BaseAttrValue> {

}




