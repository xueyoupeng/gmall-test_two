package com.atstudent.gmall.product.controller;/*
 * @author: XueYouPeng
 * @time: 23.9.6 上午 9:54
 */


import com.atstudent.gmall.common.result.Result;
import com.atstudent.gmall.common.result.ResultCodeEnum;
import com.atstudent.gmall.product.entity.BaseAttrValue;
import com.atstudent.gmall.product.service.BaseAttrValueService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/admin/product")
@Api(tags = "平台属性值的查询")
public class BaseAttrValueController {

    @Autowired
    private BaseAttrValueService baseAttrValueService;


    /*
     * 根据平台属性ID获取平台属性对象数据
     * 修改前的回显
     * @author: XueYouPeng
     * @time: 23.9.6 上午 9:55
     */
     @GetMapping(value = "/getAttrValueList/{attrId}")
     public Result<List<BaseAttrValue>> findByAttrId(@PathVariable(value = "attrId") Long attrId){
         List<BaseAttrValue> baseAttrValueList = baseAttrValueService.findByAttrId(attrId);
         return Result.build(baseAttrValueList, ResultCodeEnum.SUCCESS);
     }
}
