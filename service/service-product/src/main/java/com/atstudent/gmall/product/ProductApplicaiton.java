package com.atstudent.gmall.product;/*
 * @author: XueYouPeng
 * @time: 23.9.4 下午 8:22
 */

import com.atstudent.gmall.common.anno.EnableMinioClient;
import com.atstudent.gmall.common.anno.EnableSwagger2Configuration;
import com.atstudent.gmall.common.cache.anno.EnableRedissonClient;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;


@SpringBootApplication
@MapperScan(basePackages = "com.atstudent.gmall.product.mapper")
@EnableMinioClient
@EnableSwagger2Configuration
//开启定时任务
@EnableScheduling
@EnableRedissonClient
@EnableFeignClients(basePackages = {
        "com.atstudent.gmall.common.feign.search"
})
public class ProductApplicaiton {

    public static void main(String[] args) {
        SpringApplication.run(ProductApplicaiton.class,args);
    }
}
