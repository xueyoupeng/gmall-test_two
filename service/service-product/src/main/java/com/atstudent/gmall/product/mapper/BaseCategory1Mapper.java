package com.atstudent.gmall.product.mapper;

import com.atstudent.gmall.product.entity.BaseCategory1;
import com.atstudent.gmall.product.vo.CategoryView;
import com.atstudent.gmall.product.vo.CategoryVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
* @author xueyoupeng
* @description 针对表【base_category1(一级分类表)】的数据库操作Mapper
* @createDate 2023-09-05 14:39:24
* @Entity com.atstudent.gmall.product.entity.BaseCategory1
*/
public interface BaseCategory1Mapper extends BaseMapper<BaseCategory1> {

    public abstract List<CategoryVo> findAllCategoryTree();

    public abstract CategoryView findCategoryViewBySkuId(Long skuId);
}




