package com.atstudent.gmall.product.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atstudent.gmall.product.entity.SkuImage;
import com.atstudent.gmall.product.service.SkuImageService;
import com.atstudent.gmall.product.mapper.SkuImageMapper;
import org.springframework.stereotype.Service;

/**
* @author xueyoupeng
* @description 针对表【sku_image(库存单元图片表)】的数据库操作Service实现
* @createDate 2023-09-05 14:39:24
*/
@Service
public class SkuImageServiceImpl extends ServiceImpl<SkuImageMapper, SkuImage>
    implements SkuImageService{

}




