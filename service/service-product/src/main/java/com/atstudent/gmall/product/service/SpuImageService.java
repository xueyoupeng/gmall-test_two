package com.atstudent.gmall.product.service;

import com.atstudent.gmall.product.entity.SpuImage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author xueyoupeng
* @description 针对表【spu_image(商品图片表)】的数据库操作Service
* @createDate 2023-09-05 14:39:24
*/
public interface SpuImageService extends IService<SpuImage> {

    public abstract List<SpuImage> findBySpuId(Long spuId);
}
