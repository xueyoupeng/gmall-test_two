package com.atstudent.gmall.product.service;

import com.atstudent.gmall.product.entity.SpuPoster;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author xueyoupeng
* @description 针对表【spu_poster(商品海报表)】的数据库操作Service
* @createDate 2023-09-05 14:39:24
*/
public interface SpuPosterService extends IService<SpuPoster> {

}
