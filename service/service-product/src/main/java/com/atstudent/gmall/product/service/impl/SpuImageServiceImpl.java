package com.atstudent.gmall.product.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atstudent.gmall.product.entity.SpuImage;
import com.atstudent.gmall.product.service.SpuImageService;
import com.atstudent.gmall.product.mapper.SpuImageMapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author xueyoupeng
* @description 针对表【spu_image(商品图片表)】的数据库操作Service实现
* @createDate 2023-09-05 14:39:24
*/
@Service
public class SpuImageServiceImpl extends ServiceImpl<SpuImageMapper, SpuImage>
    implements SpuImageService{

    @Override
    public List<SpuImage> findBySpuId(Long spuId) {

        QueryWrapper<SpuImage> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("spu_id",spuId);

        List<SpuImage> spuImageList = this.list(queryWrapper);

        return spuImageList;
    }
}




