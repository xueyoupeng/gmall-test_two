package com.atstudent.gmall.product.mapper;

import com.atstudent.gmall.product.entity.BaseSaleAttr;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author xueyoupeng
* @description 针对表【base_sale_attr(基本销售属性表)】的数据库操作Mapper
* @createDate 2023-09-05 14:39:24
* @Entity com.atstudent.gmall.product.entity.BaseSaleAttr
*/
public interface BaseSaleAttrMapper extends BaseMapper<BaseSaleAttr> {

}




