package com.atstudent.gmall.product.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atstudent.gmall.product.entity.SkuAttrValue;
import com.atstudent.gmall.product.service.SkuAttrValueService;
import com.atstudent.gmall.product.mapper.SkuAttrValueMapper;
import org.springframework.stereotype.Service;

/**
* @author xueyoupeng
* @description 针对表【sku_attr_value(sku平台属性值关联表)】的数据库操作Service实现
* @createDate 2023-09-05 14:39:24
*/
@Service
public class SkuAttrValueServiceImpl extends ServiceImpl<SkuAttrValueMapper, SkuAttrValue>
    implements SkuAttrValueService{

}




