package com.atstudent.gmall.product.mapper;

import com.atstudent.gmall.product.entity.SkuImage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author xueyoupeng
* @description 针对表【sku_image(库存单元图片表)】的数据库操作Mapper
* @createDate 2023-09-05 14:39:24
* @Entity com.atstudent.gmall.product.entity.SkuImage
*/
public interface SkuImageMapper extends BaseMapper<SkuImage> {

}




