package com.atstudent.gmall.product.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atstudent.gmall.product.entity.BaseCategory3;
import com.atstudent.gmall.product.service.BaseCategory3Service;
import com.atstudent.gmall.product.mapper.BaseCategory3Mapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author xueyoupeng
* @description 针对表【base_category3(三级分类表)】的数据库操作Service实现
* @createDate 2023-09-05 14:39:24
*/
@Service
public class BaseCategory3ServiceImpl extends ServiceImpl<BaseCategory3Mapper, BaseCategory3>
    implements BaseCategory3Service{

    @Override
    public List<BaseCategory3> findBaseCategory3ByC2Id(Long category2Id) {

        LambdaQueryWrapper<BaseCategory3> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(BaseCategory3::getCategory2Id,category2Id);

        return this.list(lambdaQueryWrapper);
    }
}




