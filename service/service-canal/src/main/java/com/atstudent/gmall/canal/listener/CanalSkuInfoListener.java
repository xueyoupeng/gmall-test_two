package com.atstudent.gmall.canal.listener;/*
 * @author: XueYouPeng
 * @time: 23.9.9 下午 9:24
 */

import com.alibaba.otter.canal.protocol.CanalEntry;
import com.atstudent.gmall.common.constant.GmallConstant;
import com.xpand.starter.canal.annotation.CanalEventListener;
import com.xpand.starter.canal.annotation.ListenPoint;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.List;

// 声明当前这类是canal的一个事件监听器
@Slf4j
@CanalEventListener
public class CanalSkuInfoListener {

    @Autowired
    private RedisTemplate<String,String> redisTemplate;

    @ListenPoint(destination = "sku_canel", schema = "gmall_product", table = {"sku_info"})
    public void listenSkuInfoChange(CanalEntry.EventType eventType, CanalEntry.RowData rowData) {

        //获取变更之前的数据列
        List<CanalEntry.Column> beforeColumnsList = rowData.getBeforeColumnsList();
        beforeColumnsList.stream().forEach(column -> {
            String columnName = column.getName();
            String columnValue = column.getValue();
            log.info("测试代码")
        });


        //获取变更之后的数据列
        List<CanalEntry.Column> afterColumnsList = rowData.getAfterColumnsList();
        afterColumnsList.stream().forEach(column -> {
            String columnName = column.getName();
            if ("id".equals(columnName)){
                String columnValue = column.getValue();
                redisTemplate.delete(GmallConstant.REDSI_SKU_DETAIL_PREFIX + columnValue);
            }
        });
    }

    @ListenPoint(destination = "sku_canel", schema = "gmall_activity", table = {"sku_info"})
    public void listenactivityChange(CanalEntry.EventType eventType, CanalEntry.RowData rowData) {

    }
}
