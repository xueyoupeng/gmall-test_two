package com.atstudent.gmall.canal;/*
 * @author: XueYouPeng
 * @time: 23.9.9 下午 9:23
 */

import com.xpand.starter.canal.annotation.CanalEventListener;
import com.xpand.starter.canal.annotation.EnableCanalClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
// 声明当前的服务是Canal的一个客户端
@EnableCanalClient
public class CanalClientApplication {
    public static void main(String[] args) {
        SpringApplication.run(CanalClientApplication.class,args);

    }
}
