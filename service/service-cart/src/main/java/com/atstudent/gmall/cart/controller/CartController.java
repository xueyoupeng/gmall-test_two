package com.atstudent.gmall.cart.controller;/*
 * @author: XueYouPeng
 * @time: 23.9.15 下午 3:41
 */

import com.atstudent.gmall.cart.entity.CartItem;
import com.atstudent.gmall.cart.service.CartService;
import com.atstudent.gmall.common.result.Result;
import com.atstudent.gmall.common.result.ResultCodeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/cart")
public class CartController {

    @Autowired
    private CartService cartService;

    /*
     * 商品数量修改
     * @author: XueYouPeng
     * @time: 23.7.26 下午 8:42
     */
    @PostMapping(value = "/addToCart/{skuId}/{skuNum}")
    public Result addToCart(@PathVariable(value = "skuId") Long skuId , @PathVariable(value = "skuNum") Integer skuNum){
        cartService.addToCart(skuId,skuNum);
        return Result.ok() ;
    }

    /*
     * 商品选中不选中
     * @author: XueYouPeng
     * @time: 23.7.26 下午 8:46
     */
    @GetMapping(value = "/checkCart/{skuId}/{isChecked}")
    public Result checkCart(@PathVariable(value = "skuId") Long skuId , @PathVariable(value = "isChecked") Integer isChecked){
        cartService.checkCart(skuId , isChecked);
        return Result.ok();
    }

    /*
     * 购物车商品移除
     * @author: XueYouPeng
     * @time: 23.7.26 下午 9:00
     */
    @DeleteMapping(value = "/deleteCart/{skuId}")
    public Result deleteCart(@PathVariable(value = "skuId") Long skuId){
        cartService.deleteCart(skuId);
        return Result.ok();
    }

    /*
     * 查询购物车中的所有商品
     * @author: XueYouPeng
     * @time: 23.7.26 下午 8:43
     */
    @GetMapping(value = "/cartList")
    public Result<List<CartItem>> findAllCartItem() {
        List<CartItem> cartItemList = cartService.findAllCartItem() ;
        return Result.build(cartItemList , ResultCodeEnum.SUCCESS) ;
    }
}
