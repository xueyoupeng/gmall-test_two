package com.atstudent.gmall.cart.biz;/*
 * @author: XueYouPeng
 * @time: 23.9.15 上午 10:42
 */

import com.atstudent.gmall.cart.entity.CartItem;
import com.atstudent.gmall.cart.vo.AddCartSuccessVo;

import java.util.List;

public interface CartBizService {
    public abstract AddCartSuccessVo addCart(Long skuId, Integer skuNum);

    public abstract void deleteAllChecked();

    public abstract List<CartItem> findAllSelectedCartItem();
}
