package com.atstudent.gmall.cart.service;/*
 * @author: XueYouPeng
 * @time: 23.9.15 下午 3:41
 */

import com.atstudent.gmall.cart.entity.CartItem;

import java.util.List;

public interface CartService {
    public abstract void addToCart(Long skuId, Integer skuNum);

    public abstract void checkCart(Long skuId, Integer isChecked);

    public abstract void deleteCart(Long skuId);

    public abstract List<CartItem> findAllCartItem();
}
