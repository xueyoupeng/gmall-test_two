package com.atstudent.gmall.cart.rpc;/*
 * @author: XueYouPeng
 * @time: 23.9.15 上午 10:41
 */

import com.atstudent.gmall.cart.biz.CartBizService;
import com.atstudent.gmall.cart.entity.CartItem;
import com.atstudent.gmall.cart.vo.AddCartSuccessVo;
import com.atstudent.gmall.common.result.Result;
import com.atstudent.gmall.common.result.ResultCodeEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/inner/cart")
@Slf4j
public class CartRpcController {

    @Autowired
    private CartBizService cartBizService;

    /*
     * 添加商品到购物车
     * @author: XueYouPeng
     * @time: 23.9.15 上午 10:43
     */
     @GetMapping(value = "/addCart")
     public Result<AddCartSuccessVo> addCart(
             @RequestParam(value = "skuId") Long skuId ,
             @RequestParam(value = "skuNum")Integer skuNum
     ){
         AddCartSuccessVo addCartSuccessVo = cartBizService.addCart(skuId , skuNum);
         return Result.build(addCartSuccessVo , ResultCodeEnum.SUCCESS);
     }

    /*
     * 删除选中的购物车中的数据
     * @author: XueYouPeng
     * @time: 23.7.26 下午 9:27
     */
    @DeleteMapping(value = "/deleteAllChecked")
    public Result deleteAllChecked(){
        cartBizService.deleteAllChecked() ;
        return Result.ok() ;
    }

    /*
     * 购物车中选中的商品 供订单微服务调用
     * @author: XueYouPeng
     * @time: 23.9.16 下午 9:50
     */
    @GetMapping(value = "/findAllSelectedCartItem")
    public Result<List<CartItem>> findAllSelectedCartItem(){
        List<CartItem> cartItemList = cartBizService.findAllSelectedCartItem();
        return Result.build(cartItemList , ResultCodeEnum.SUCCESS);
    }


}
