package com.atstudent.gmall.product.dto;/*
 * @author: XueYouPeng
 * @time: 23.7.13 上午 11:25
 */

import com.atstudent.gmall.product.entity.SpuImage;
import com.atstudent.gmall.product.entity.SpuSaleAttr;
import lombok.Data;

import java.util.List;

@Data
public class SpuInfoDto {

    private Long id ;
    private String spuName ;
    private String description ;
    private Long category3Id ;
    private Long tmId ;

    private List<SpuImage> spuImageList;
    private List<SpuSaleAttr> spuSaleAttrList;
}
