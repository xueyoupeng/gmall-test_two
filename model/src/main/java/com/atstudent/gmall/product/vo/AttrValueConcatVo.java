package com.atstudent.gmall.product.vo;

import lombok.Data;

@Data
public class AttrValueConcatVo {

    private Long skuId ;
    private String attrValueConcat ;

}
