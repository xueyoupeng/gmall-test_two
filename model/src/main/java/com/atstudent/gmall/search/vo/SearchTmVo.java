package com.atstudent.gmall.search.vo;

import lombok.Data;

@Data
public class SearchTmVo {

    private Long tmId ;
    private String tmName ;
    private String tmLogoUrl ;

}
