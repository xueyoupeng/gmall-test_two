package com.atstudent.gmall.cart.vo;

import com.atstudent.gmall.product.entity.SkuInfo;
import lombok.Data;

@Data
public class AddCartSuccessVo {

    private SkuInfo skuInfo ;
    private Integer skuNum ;

}
