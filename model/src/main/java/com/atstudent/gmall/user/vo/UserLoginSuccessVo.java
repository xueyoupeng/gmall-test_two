package com.atstudent.gmall.user.vo;

import lombok.Data;

@Data
public class UserLoginSuccessVo {

    private String token ;
    private String nickName ;

}
