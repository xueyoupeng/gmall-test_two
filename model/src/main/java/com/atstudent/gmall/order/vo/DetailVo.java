package com.atstudent.gmall.order.vo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class DetailVo {

    //imgUrl、skuName、orderPrice、skuNum、skuId
    private String imgUrl ;
    private String skuName;
    private BigDecimal orderPrice;
    private Integer skuNum;
    private Long skuId ;

    //是否有货标志： 1：有货
    private String hasStock = "1";

}
