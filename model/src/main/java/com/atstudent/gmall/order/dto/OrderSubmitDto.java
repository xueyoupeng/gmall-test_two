package com.atstudent.gmall.order.dto;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
public class OrderSubmitDto {

    @NotBlank(message = "收件人名称不能为空")
    private String consignee ;      // 收件人名称

    @NotBlank(message = "收件人的手机号码不能为空")
    @Length(min = 3 , max = 11 , message = "手机号码格式不正确")
    private String consigneeTel ;   // 收件人的手机号码

    @NotBlank(message = "收件人的地址信息不能为空")
    private String deliveryAddress ;    // 收件人的地址信息

    private String orderComment ;       // 订单的备注

    private List<DetailDto> orderDetailList ;     // 订单明细数据

}
