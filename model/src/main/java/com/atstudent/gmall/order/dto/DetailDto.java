package com.atstudent.gmall.order.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class DetailDto {

    private String imgUrl ;
    private String skuName ;
    private BigDecimal orderPrice ;
    private Integer skuNum ;
    private Long skuId ;
    private Integer hasStock ;

}
