package com.atstudent.gmall.common.feign.product.fallback;/*
 * @author: XueYouPeng
 * @time: 23.9.7 上午 8:53
 */

import com.atstudent.gmall.common.feign.product.BaseCategoryFeignClient;
import com.atstudent.gmall.common.result.Result;
import com.atstudent.gmall.common.result.ResultCodeEnum;
import com.atstudent.gmall.product.vo.CategoryVo;

import java.util.List;

public class BaseCategoryFeignClientFallback implements BaseCategoryFeignClient {
    @Override
    public Result<List<CategoryVo>> findAllCategoryTree() {
        return Result.build(null, ResultCodeEnum.SUCCESS);

    }
}
