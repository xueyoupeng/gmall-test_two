package com.atstudent.gmall.common.feign.util;/*
 * @author: XueYouPeng
 * @time: 23.9.15 下午 12:12
 */

import com.atstudent.gmall.common.feign.interceptor.AuthUserInfo;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

public class AuthUserInfoUtils {

    public static AuthUserInfo getAuthUserInfo(){

        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (servletRequestAttributes != null){

            //获取用户的id和临时用户的id 拿到请求头
            HttpServletRequest httpServletRequest = servletRequestAttributes.getRequest();
            String userId = httpServletRequest.getHeader("userId");
            String userTempId = httpServletRequest.getHeader("userTempId");

            //封装数据
            AuthUserInfo authUserInfo = new AuthUserInfo();
            authUserInfo.setUserId(userId);
            authUserInfo.setUserTempId(userTempId);
            return authUserInfo;
        }
        return null;
    }
}
