package com.atstudent.gmall.common.feign.interceptor;/*
 * @author: XueYouPeng
 * @time: 23.9.15 下午 12:09
 */

import com.atstudent.gmall.common.feign.util.AuthUserInfoUtils;
import feign.RequestInterceptor;
import feign.RequestTemplate;

public class FeignClientInterceptor implements RequestInterceptor {

    @Override
    public void apply(RequestTemplate template) {


        // 从RequestContextHolder中获取HttpServletRequest对象
        //调用工具类AuthUserInfoUtils
        AuthUserInfo authUserInfo = AuthUserInfoUtils.getAuthUserInfo();
        if (authUserInfo != null){
            template.header("userId" , authUserInfo.getUserId());
            template.header("userTempId" , authUserInfo.getUserTempId());
        }
    }
}
