package com.atstudent.gmall.common.feign.cart.fallback;/*
 * @author: XueYouPeng
 * @time: 23.9.15 上午 10:46
 */

import com.atstudent.gmall.cart.entity.CartItem;
import com.atstudent.gmall.cart.vo.AddCartSuccessVo;
import com.atstudent.gmall.common.feign.cart.CartFeignClient;
import com.atstudent.gmall.common.result.Result;
import com.atstudent.gmall.common.result.ResultCodeEnum;

import java.util.List;

public class CartFeignClientFallback implements CartFeignClient {
    @Override
    public Result<AddCartSuccessVo> addCart(Long skuId, Integer skuNum) {
        return Result.build(null, ResultCodeEnum.SUCCESS);
    }

    @Override
    public Result deleteAllChecked() {
        return Result.build(null, ResultCodeEnum.SUCCESS);
    }

    @Override
    public Result<List<CartItem>> findAllSelectedCartItem() {
        return Result.build(null, ResultCodeEnum.SUCCESS);
    }
}
