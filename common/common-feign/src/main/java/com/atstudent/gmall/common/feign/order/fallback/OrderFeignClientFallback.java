package com.atstudent.gmall.common.feign.order.fallback;/*
 * @author: XueYouPeng
 * @time: 23.9.16 下午 6:22
 */

import com.atstudent.gmall.common.feign.order.OrderFeignClient;
import com.atstudent.gmall.common.result.Result;
import com.atstudent.gmall.common.result.ResultCodeEnum;
import com.atstudent.gmall.order.entity.OrderInfo;
import com.atstudent.gmall.order.vo.OrderConfirmVo;

public class OrderFeignClientFallback implements OrderFeignClient {
    @Override
    public Result<OrderConfirmVo> submitOrder() {
        return Result.build(null , ResultCodeEnum.SUCCESS);
    }

    @Override
    public Result<OrderInfo> findOrderInfo(Long orderId) {
        return Result.build(null , ResultCodeEnum.SUCCESS);

    }
}
