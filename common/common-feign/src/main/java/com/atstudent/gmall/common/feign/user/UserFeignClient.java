package com.atstudent.gmall.common.feign.user;

import com.atstudent.gmall.common.result.Result;
import com.atstudent.gmall.user.entity.UserAddress;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient(value = "service-user")
public interface UserFeignClient {

    @GetMapping(value = "/api/inner/user/findUserAddressByUserId")
    public Result<List<UserAddress>> findUserAddressByUserId() ;

}
