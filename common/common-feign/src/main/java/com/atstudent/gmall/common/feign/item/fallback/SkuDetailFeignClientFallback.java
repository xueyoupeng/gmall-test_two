package com.atstudent.gmall.common.feign.item.fallback;/*
 * @author: XueYouPeng
 * @time: 23.9.7 下午 12:23
 */

import com.atstudent.gmall.common.feign.item.SkuDetailFeignClient;
import com.atstudent.gmall.common.result.Result;
import com.atstudent.gmall.common.result.ResultCodeEnum;
import com.atstudent.gmall.product.vo.SkuDetailVo;

public class SkuDetailFeignClientFallback implements SkuDetailFeignClient {
    @Override
    public Result<SkuDetailVo> item(Long skuId) {
        return Result.build(null , ResultCodeEnum.SUCCESS);

    }
}
