package com.atstudent.gmall.common.feign.search.fallback;/*
 * @author: XueYouPeng
 * @time: 23.9.12 下午 2:03
 */

import com.atstudent.gmall.common.feign.search.SearchFeignClient;
import com.atstudent.gmall.common.result.Result;
import com.atstudent.gmall.common.result.ResultCodeEnum;
import com.atstudent.gmall.search.dto.SearchParamDTO;
import com.atstudent.gmall.search.entity.Goods;
import com.atstudent.gmall.search.vo.SearchResponseVo;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SearchFeignClientFallback implements SearchFeignClient {

    @Override
    public Result saveGoods(Goods goods) {
        log.info("SearchFeignClientFallback...saveGoods方法执行了...");
        return Result.build(null , ResultCodeEnum.SUCCESS) ;
    }

    @Override
    public Result deleteById(Long skuId) {
        log.info("SearchFeignClientFallback...deleteById方法执行了...");
        return Result.build(null , ResultCodeEnum.SUCCESS) ;
    }

    @Override
    public Result<SearchResponseVo> search(SearchParamDTO searchParamDTO) {
        log.info("SearchFeignClientFallback...search方法执行了...");
        return Result.build(null , ResultCodeEnum.SUCCESS) ;
    }

    @Override
    public Result updateHotScore(Long skuId, Long hotScore) {
        log.info("SearchFeignClientFallback...updateHotScore方法执行了...");
        return Result.build(null , ResultCodeEnum.SUCCESS) ;
    }
}
