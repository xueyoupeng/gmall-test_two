package com.atstudent.gmall.common.feign.cart;/*
 * @author: XueYouPeng
 * @time: 23.9.15 上午 10:46
 */

import com.atstudent.gmall.cart.entity.CartItem;
import com.atstudent.gmall.cart.vo.AddCartSuccessVo;
import com.atstudent.gmall.common.feign.cart.fallback.CartFeignClientFallback;
import com.atstudent.gmall.common.result.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(value = "service-cart" , fallback = CartFeignClientFallback.class)
public interface CartFeignClient {

    @GetMapping(value = "/api/inner/cart/addCart")
    public Result<AddCartSuccessVo> addCart(@RequestParam(value = "skuId") Long skuId ,
                                            @RequestParam(value = "skuNum")Integer skuNum ) ;

    @DeleteMapping(value = "/api/inner/cart/deleteAllChecked")
    public Result deleteAllChecked();

    @GetMapping(value = "/api/inner/cart/findAllSelectedCartItem")
    public Result<List<CartItem>> findAllSelectedCartItem() ;
}
