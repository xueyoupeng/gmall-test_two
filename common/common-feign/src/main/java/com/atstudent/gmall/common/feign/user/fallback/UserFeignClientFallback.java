package com.atstudent.gmall.common.feign.user.fallback;/*
 * @author: XueYouPeng
 * @time: 23.7.28 下午 6:25
 */


import com.atstudent.gmall.common.feign.user.UserFeignClient;
import com.atstudent.gmall.common.result.Result;
import com.atstudent.gmall.common.result.ResultCodeEnum;
import com.atstudent.gmall.user.entity.UserAddress;

import java.util.List;

public class UserFeignClientFallback implements UserFeignClient {
    @Override
    public Result<List<UserAddress>> findUserAddressByUserId() {
        return Result.build(null , ResultCodeEnum.SUCCESS);

    }
}
