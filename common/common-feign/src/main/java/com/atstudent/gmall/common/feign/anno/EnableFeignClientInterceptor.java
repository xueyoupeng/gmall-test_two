package com.atstudent.gmall.common.feign.anno;/*
 * @author: XueYouPeng
 * @time: 23.9.15 下午 12:16
 */

import com.atstudent.gmall.common.feign.interceptor.FeignClientInterceptor;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(value = RetentionPolicy.RUNTIME)
@Target(value = ElementType.TYPE)
@Import(value = FeignClientInterceptor.class)
public @interface EnableFeignClientInterceptor {
}
