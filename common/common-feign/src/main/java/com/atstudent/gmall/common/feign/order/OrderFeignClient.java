package com.atstudent.gmall.common.feign.order;/*
 * @author: XueYouPeng
 * @time: 23.9.16 下午 6:21
 */

import com.atstudent.gmall.common.feign.order.fallback.OrderFeignClientFallback;
import com.atstudent.gmall.common.result.Result;
import com.atstudent.gmall.order.entity.OrderInfo;
import com.atstudent.gmall.order.vo.OrderConfirmVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "service-order" , fallback = OrderFeignClientFallback.class)
public interface OrderFeignClient {

    @GetMapping(value = "/api/inner/order/submitOrder")
    public Result<OrderConfirmVo> submitOrder();

    @GetMapping(value = "/api/inner/order/findOrderInfo/{orderId}")
    public Result<OrderInfo> findOrderInfo(@PathVariable(value = "orderId") Long orderId) ;
}
