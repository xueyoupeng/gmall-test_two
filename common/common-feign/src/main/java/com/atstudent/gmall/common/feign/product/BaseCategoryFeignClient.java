package com.atstudent.gmall.common.feign.product;/*
 * @author: XueYouPeng
 * @time: 23.9.7 上午 8:52
 */

import com.atstudent.gmall.common.feign.product.fallback.BaseCategoryFeignClientFallback;
import com.atstudent.gmall.common.result.Result;
import com.atstudent.gmall.product.vo.CategoryVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient(value = "service-product", fallback = BaseCategoryFeignClientFallback.class)
public interface BaseCategoryFeignClient {

    @GetMapping(value = "/api/inner/product/findAllCategoryTree")
    public abstract Result<List<CategoryVo>> findAllCategoryTree();
}
