package com.atstudent.gmall.common.feign.search;/*
 * @author: XueYouPeng
 * @time: 23.9.12 下午 2:03
 */

import com.atstudent.gmall.common.feign.search.fallback.SearchFeignClientFallback;
import com.atstudent.gmall.common.result.Result;
import com.atstudent.gmall.search.dto.SearchParamDTO;
import com.atstudent.gmall.search.entity.Goods;
import com.atstudent.gmall.search.vo.SearchResponseVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(value = "service-search" , fallback = SearchFeignClientFallback.class)
public interface SearchFeignClient {

    @PostMapping(value = "/api/inner/search/saveGoods")
    public Result saveGoods(@RequestBody Goods goods);

    @DeleteMapping(value = "/api/inner/search/deleteById/{id}")
    public Result deleteById(@PathVariable(value = "id") Long skuId);

    @PostMapping(value = "/api/inner/search/search")
    public Result<SearchResponseVo> search(@RequestBody SearchParamDTO searchParamDTO);

    @PutMapping(value = "/api/inner/search/updateHotScore/{skuId}/{hotScore}")
    public Result updateHotScore(@PathVariable(value = "skuId") Long skuId , @PathVariable(value = "hotScore")Long hotScore) ;
}
