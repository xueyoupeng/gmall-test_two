package com.atstudent.gmall.common.feign.product.fallback;/*
 * @author: XueYouPeng
 * @time: 23.9.7 下午 2:08
 */

import com.atstudent.gmall.common.feign.product.SkuFeignClient;
import com.atstudent.gmall.common.result.Result;
import com.atstudent.gmall.common.result.ResultCodeEnum;
import com.atstudent.gmall.product.entity.SkuInfo;
import com.atstudent.gmall.product.entity.SpuSaleAttr;
import com.atstudent.gmall.product.vo.AttrValueConcatVo;
import com.atstudent.gmall.product.vo.CategoryView;
import com.atstudent.gmall.product.vo.SkuDetailVo;

import java.util.List;

public class SkuFeignClientFallback implements SkuFeignClient {
    @Override
    public Result<CategoryView> findCategoryViewBySkuId(Long skuId) {
        return Result.build(null , ResultCodeEnum.SUCCESS) ;

    }

    @Override
    public Result<SkuInfo> findSkuInfoAndImageBySkuId(Long skuId) {
        return Result.build(null , ResultCodeEnum.SUCCESS) ;

    }

    @Override
    public Result<SkuInfo> findSkuInfoBySkuId(Long skuId) {
        return Result.build(null , ResultCodeEnum.SUCCESS) ;

    }

    @Override
    public Result<List<SpuSaleAttr>> findSpuSalAttrBySkuId(Long skuId) {
        return Result.build(null , ResultCodeEnum.SUCCESS) ;
    }

    @Override
    public Result<List<AttrValueConcatVo>> findSkuAttrValueConcatBySkuId(Long skuId) {
        return Result.build(null , ResultCodeEnum.SUCCESS) ;
    }

    @Override
    public Result<List<Long>> findAllSkuIds() {
        return Result.build(null , ResultCodeEnum.SUCCESS) ;

    }

    @Override
    public Result<SkuDetailVo> findSkuDetailVo(Long skuId) {
        return Result.build(null , ResultCodeEnum.SUCCESS) ;

    }
}
