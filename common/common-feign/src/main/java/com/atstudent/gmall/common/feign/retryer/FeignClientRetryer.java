package com.atstudent.gmall.common.feign.retryer;/*
 * @author: XueYouPeng
 * @time: 23.9.18 上午 10:59
 */

import feign.RetryableException;
import feign.Retryer;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FeignClientRetryer implements Retryer {

    //这个类是feign的自定义重试器
    //没有方法调用它 他是配置在web-all的yml配置文件中

    private int start = 1;
    private int end = 3;

    @Override
    public void continueOrPropagate(RetryableException e) {
        log.info("FeignClientRetryer....continueOrPropagate方法执行了...");
        if (start >= end){
            throw e;
        }
        start++;
    }

    @Override
    public Retryer clone() { // 在clone方法中返回一个重试器对象
        return new FeignClientRetryer();
    }
}
