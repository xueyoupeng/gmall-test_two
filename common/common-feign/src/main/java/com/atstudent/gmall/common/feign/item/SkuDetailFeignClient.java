package com.atstudent.gmall.common.feign.item;/*
 * @author: XueYouPeng
 * @time: 23.9.7 下午 12:22
 */

import com.atstudent.gmall.common.feign.item.fallback.SkuDetailFeignClientFallback;
import com.atstudent.gmall.common.result.Result;
import com.atstudent.gmall.product.vo.SkuDetailVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "service-item",fallback = SkuDetailFeignClientFallback.class)
public interface SkuDetailFeignClient {

    @GetMapping(value = "/api/inner/item/item/{skuId}")
    public Result<SkuDetailVo> item(@PathVariable(value = "skuId") Long skuId);

}
