package com.atstudent.gmall.common.feign.ware;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "ware-manage" , url = "http://localhost:9001")
public interface WareFeignClient {

    @GetMapping(value = "/hasStock")
    public abstract String hasStock(@RequestParam(value = "skuId") Long skuId , @RequestParam(value = "num") Integer num) ;

}
