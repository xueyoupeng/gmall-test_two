package com.atstudent.gmall.rabbit.configuration;/*
 * @author: XueYouPeng
 * @time: 23.9.18 下午 8:00
 */

import com.atstudent.gmall.rabbit.constant.RabbitConstant;
import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OrderDelayQueueConfiguration {

    @Bean//交换机
    public Exchange exchange(){
        // durable方法参数设置为true表示交换机需要进行持久化
        Exchange exchange = ExchangeBuilder.directExchange(RabbitConstant.ORDER_EXCHANGE).durable(true).build();
        return exchange;
    }

    //生成订单的队列
    @Bean
    public Queue orderQueue(){
        Queue queue = QueueBuilder.durable(RabbitConstant.ORDER_QUEUE)
                //绑定死信交换机  --》用上面那个交换机最为死信交换机
                .deadLetterExchange(RabbitConstant.ORDER_EXCHANGE)
                //绑定死信交换机的routingKey
                .deadLetterRoutingKey(RabbitConstant.DEAD_LETTER_ROUTING_KEY)
                .ttl(1000 * 60 * 30)
                .build();
        return queue;
    }

    //绑定交换机和生成订单的队列
    @Bean
    public Binding orderQueueBinding(){
        //队列 to->交换机
        Binding binding = BindingBuilder.bind(orderQueue()).to(exchange()).with(RabbitConstant.ORDER_ROUTING_KEY).noargs();
        return binding;
    }

    //死信队列
    @Bean
    public Queue orderDelayQueue(){
        Queue queue = QueueBuilder.durable(RabbitConstant.ORDER_DELAY_QUEUE).build();
        return queue;
    }

    //绑定交换机和死信队列
    @Bean
    public Binding orderDelayQueueBinding(){
        Binding binding = BindingBuilder.bind(orderDelayQueue()).to(exchange()).with(RabbitConstant.DEAD_LETTER_ROUTING_KEY).noargs();
        return binding;
    }

}
