package com.atstudent.gmall.rabbit.anno;/*
 * @author: XueYouPeng
 * @time: 23.9.18 下午 8:14
 */

import com.atstudent.gmall.rabbit.configuration.OrderDelayQueueConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(value = RetentionPolicy.RUNTIME)
@Target(value = ElementType.TYPE)
@Import(value = OrderDelayQueueConfiguration.class)
public @interface EnableOrderDelayQueue {
}
