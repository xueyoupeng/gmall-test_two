package com.atstudent.gmall.rabbit.constant;

// 和Rabbitmq相关的常量数据
public interface RabbitConstant {

    /**
     * 订单关闭订单的交换机
     */
    public static final String ORDER_EXCHANGE = "order.exchange" ;

    /**
     * 订单关闭订单的队列名称
     */
    public static final String ORDER_QUEUE = "order.queue" ;

    /**
     * 死信的routingKey
     */
    public static final String DEAD_LETTER_ROUTING_KEY = "order.delay" ;

    /**
     * 订单发送订单延迟消息的routingKey
     */
    public static final String ORDER_ROUTING_KEY = "order.info" ;

    /**
     * 订单死信队列的名称
     */
    public static final String ORDER_DELAY_QUEUE = "order.delay.queue" ;

    /**
     * 支付成功以后更改订单状态的队列
     */
    public static final String ORDER_PAYED_QUEUE = "order.payed.queue" ;

    /**
     * 支付成功以后更改订单状态的routingKey
     */
    public static final String ORDER_PAYED_ROUTING_KEY = "order.payed" ;

    /**
     * 扣减库存的交换机的名称
     */
    public static final String WARE_STOCK_EXCHANGE = "exchange.direct.ware.stock" ;

    /**
     * 扣减库存的消息的routingKey
     */
    public static final String WARE_STOCK_ROUTING_KEY = "ware.stock" ;

    /**
     * 秒杀下单的交换机的名称
     */
    public static final String SECKILL_ORDER_EXCHANGE = "seckill.order.exchange" ;

    /**
     * 秒杀下单的队列的名称
     */
    public static final String SECKILL_ORDER_QUEUE = "seckill.order.queue" ;

    /**
     * 秒杀下单的队列的routingKey
     */
    public static final String SECKILL_ORDER_ROUTING_KEY = "seckill.order" ;

}
