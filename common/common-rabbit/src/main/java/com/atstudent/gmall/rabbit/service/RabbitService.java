package com.atstudent.gmall.rabbit.service;/*
 * @author: XueYouPeng
 * @time: 23.9.19 上午 8:33
 */

import com.rabbitmq.client.Channel;

public interface RabbitService {

    public abstract void retry(Channel channel , long deliveryTag , String msgContent , int count);
}
