package com.atstudent.gmall.rabbit.anno;/*
 * @author: XueYouPeng
 * @time: 23.9.18 下午 6:30
 */

import com.atstudent.gmall.rabbit.configuration.RabbitConfiguration;
import com.atstudent.gmall.rabbit.service.impl.RabbitServiceImpl;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(value = RetentionPolicy.RUNTIME)
@Target(value = ElementType.TYPE)
@Import(value = { RabbitConfiguration.class , RabbitServiceImpl.class})
public @interface EnableRabbit {
}
