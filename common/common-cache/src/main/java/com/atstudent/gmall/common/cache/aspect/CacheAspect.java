package com.atstudent.gmall.common.cache.aspect;/*
 * @author: XueYouPeng
 * @time: 23.9.10 下午 7:29
 */

import com.alibaba.fastjson.JSON;
import com.atstudent.gmall.common.cache.anno.GmallCache;
import com.atstudent.gmall.common.cache.service.RedisCacheService;
import com.atstudent.gmall.common.constant.GmallConstant;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.redisson.api.RBloomFilter;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.Expression;
import org.springframework.expression.ParserContext;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.util.StringUtils;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.concurrent.TimeUnit;

@Aspect
@Slf4j
public class CacheAspect {

    @Autowired
    private RedisCacheService redisCacheService ;

    @Autowired
    private RedissonClient redissonClient;

    @Around(value = "@annotation(gmallCache)")
    public Object around(ProceedingJoinPoint proceedingJoinPoint , GmallCache gmallCache){

        //拿到布隆过滤器名称的表达式
        String bloomFilterNameExpression = gmallCache.bloomFilterName();
        //如果不为空 说明用到了布隆过滤器
        if (!StringUtils.isEmpty(bloomFilterNameExpression)){
            //拿到布隆过滤器值的表达式
            String bloomFilterValueExpression = gmallCache.bloomFilterValue();
            if (StringUtils.isEmpty(bloomFilterValueExpression)){
                //对表达式进行解析
                String bloomFilterName = paraseExpression(proceedingJoinPoint, bloomFilterNameExpression, String.class);
                Object bloomFilterValue = paraseExpression(proceedingJoinPoint, bloomFilterValueExpression, Object.class);
                //调用布隆过滤器
                RBloomFilter<Object> bloomFilter = redissonClient.getBloomFilter(bloomFilterName);
                //这个值-bloomFilterValue  就是前端传过来的商品id  布隆过滤器中存的就是商品id
                //bloomFilterValue = "#{#params[0]}"  params这个参数就是读取传递的参数
                if (!bloomFilter.contains(bloomFilterValue)){
                    log.info("分布式的布隆过滤器中不存在对应的数据,返回null...");
                    return null;
                }
            }
        }

        // 获取redis的数据key表达式  // sku-detail:#{#params[0]}
        String cacheKeyExpression = gmallCache.cacheKey();
        String cacheKey = paraseExpression(proceedingJoinPoint, cacheKeyExpression, String.class);

        //获取目标方法的
        Type returnType = getMethodReturnType(proceedingJoinPoint);

        // 查询Redis, Redis中如果没有数据继续向下执行
        Object result = redisCacheService.getData(cacheKey, returnType);
        if (result != null){
            log.info("从缓存中查询到了数据，返回...");
            return result ;
        }

        //没有查到数据 从数据库中查
        //先拿锁
        boolean enableLock = gmallCache.enableLock();
        long time = gmallCache.time();
        TimeUnit timeUnit = gmallCache.timeUnit();
        Object noLockResult = null;//不使用锁 从数据库中查

        if (enableLock){
            //先拿锁的名称 的 表达式
            String lockNameExpression = gmallCache.lockName();
            if (!StringUtils.isEmpty(lockNameExpression)){//需要使用分布式锁
                //对锁的表达式进行解析 拿到锁的名称
                String lockName = paraseExpression(proceedingJoinPoint, lockNameExpression, String.class);
                RLock rLock = redissonClient.getLock(lockName);
                //加锁 没有使用参数 默认有看门狗机制 30s 过了1/3后，再自动续期到默认的30s
                boolean tryLock = rLock.tryLock();
                if (tryLock){//获取到了锁
                    try {
                        log.info("线程:{}获取到了锁，从数据库中查询数据..." , Thread.currentThread().getId());
                        noLockResult = executTargetMethod(proceedingJoinPoint, cacheKey, time, timeUnit);
                        return  noLockResult;
                    } catch (Exception e) {
                        e.printStackTrace();
                        //一定要抛出异常 让事务接收到 进行事务回滚 否则会事务失效
                        throw new RuntimeException(e);
                    } finally {//释放分布式锁
                        rLock.unlock();
                    }
                }else {//没有获取到锁
                    log.info("线程:{}没有获取到了锁，从redis中进行查询数据..." , Thread.currentThread().getId());
                    result = redisCacheService.getData(cacheKey , returnType);
                    if (result != null){
                        log.info("线程:{} 从缓存中查询到了数据，返回..." , Thread.currentThread().getId());
                        return result ;
                    }
                    //没有查到 就休眠一会
                    // 线程休眠500ms
                    try {
                        TimeUnit.MILLISECONDS.sleep(500);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }

                    //再去查
                    result = redisCacheService.getData(cacheKey , returnType);
                    return  result;
                }
            }else {//不需要使用分布式锁
                //没有加锁 直接从数据库中查
                noLockResult = executTargetMethod(proceedingJoinPoint, cacheKey, time, timeUnit);
                return noLockResult;
            }
        }else {//不需要使用分布式锁
            noLockResult = executTargetMethod(proceedingJoinPoint, cacheKey, time, timeUnit);
            return noLockResult;
        }
    }

    SpelExpressionParser spelExpressionParser = new SpelExpressionParser();

    //查询数据库  设置数据到Redis中的方法
    public Object executTargetMethod(ProceedingJoinPoint proceedingJoinPoint , String cacheKey , Long time , TimeUnit timeUnit){

        Object result = null;
        try {
            //拿到结果数据
            result = proceedingJoinPoint.proceed();
            if (result == null){//存X
                log.info("线程:{} , 从数据库没有查询到数据，将Redis中存储是X" , Thread.currentThread().getId());
                redisCacheService.saveData(cacheKey , GmallConstant.REDIS_NULL_VALUE , time , timeUnit);
            }else { //存真实数据
                log.info("线程:{} , 从数据库查询到了数据，然后将数据存储到Redis中" , Thread.currentThread().getId());
                redisCacheService.saveData(cacheKey , result , time , timeUnit);
            }
        } catch (Throwable e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

        //返回数据
        log.info("返回数据库中查询到的数据：{}" , JSON.toJSONString(result) );
        return result;
    }

    //动态获取类型
    public Type getMethodReturnType(ProceedingJoinPoint proceedingJoinPoint){
        MethodSignature methodSignature = (MethodSignature) proceedingJoinPoint.getSignature();
        Method method = methodSignature.getMethod();
        Type returnType = method.getGenericReturnType();
        return returnType;
    }

    // 对表达式进行解析获取结果
    public <T> T paraseExpression(ProceedingJoinPoint proceedingJoinPoint , String expressionStr , Class<T> clazz){
        Expression expression = spelExpressionParser.parseExpression(expressionStr, ParserContext.TEMPLATE_EXPRESSION);
        StandardEvaluationContext evaluationContext = new StandardEvaluationContext();
        evaluationContext.setVariable("params" , proceedingJoinPoint.getArgs());
        T value = expression.getValue(evaluationContext, clazz);
        return value;
    }
}
