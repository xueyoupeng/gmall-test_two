package com.atstudent.gmall.common.cache.service.impl;/*
 * @author: XueYouPeng
 * @time: 23.9.10 下午 1:59
 */

import com.alibaba.fastjson.JSON;
import com.atstudent.gmall.common.cache.service.RedisCacheService;
import com.atstudent.gmall.common.constant.GmallConstant;
import com.atstudent.gmall.common.execption.GmallException;
import com.atstudent.gmall.common.result.ResultCodeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import java.lang.reflect.Type;
import java.util.concurrent.TimeUnit;

public class RedisCacheServiceImpl implements RedisCacheService {

    @Autowired
    private RedisTemplate<String , String> redisTemplate;

    @Override
    public Object getData(String redisKey, Type type) {

        //从Redis中拿数据
        String redisValue = redisTemplate.opsForValue().get(redisKey);
        if (GmallConstant.REDIS_NULL_VALUE.equalsIgnoreCase(redisValue)){
            throw new GmallException(ResultCodeEnum.REDIS_DATA_ERROR);
        }
        if (null == redisValue || "".equalsIgnoreCase(redisValue)){
            return null;
        }
        Object object = JSON.parseObject(redisValue, type);
        return object;
    }

    @Override
    public void saveData(String redisKey, Object data, Long time, TimeUnit timeUnit) {

        if (data != null){
            //如果存的是X
            if (GmallConstant.REDIS_NULL_VALUE.equalsIgnoreCase(data.toString())){
                redisTemplate.opsForValue().set(redisKey , GmallConstant.REDIS_NULL_VALUE,time,timeUnit);
            }else {
                redisTemplate.opsForValue().set(redisKey,JSON.toJSONString(data),time,timeUnit);
            }
        }
    }
}
