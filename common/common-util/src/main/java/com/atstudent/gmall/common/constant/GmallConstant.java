package com.atstudent.gmall.common.constant;

public interface GmallConstant {

    /**
     * 缓存三级分类的数据key
     */
    public static final String REDIS_CATEGORY_KEY = "allCategory" ;

    /**
     * 缓存x的值
     */
    public static final String REDIS_NULL_VALUE = "X" ;

    /**
     * 商品详情的redis的key的前缀
     */
    public static final String REDSI_SKU_DETAIL_PREFIX = "sku-detail:" ;

    /**
     * 分布式锁的前缀
     */
    public static final String REDIS_ITEM_LOCK_PREFIX = "item-lock:" ;

    /**
     * 分布式布隆过滤器的名称
     */
    public static final String REDIS_SKUID_BLOOM_FILTER = "skuId-bloom-filter" ;

    /**
     * 分布式布隆过滤器新的名称
     */
    public static final String REDIS_SKUID_BLOOM_FILTER_NEW = "skuId-bloom-filter-new" ;

    /**
     * 商品热度分前缀
     */
    public static final String REDIS_SKU_HOT_SCORE_PRE = "sku-hotscore:" ;

    /**
     * 存储到Redis中的登录用户的键
     */
    public static final String REDIS_USER_LOGIN_PRE = "user-login-token:" ;

    /**
     * 存储到Redis中的购物车数据key
     */
    public static final String REDIS_CART_PRE = "cart:" ;

    /**
     * 存储外部交易号的数据key
     */
    public static final String REDIS_ORDER_CONFIRM = "order:confirm:" ;

    /*
     * 秒杀商品的前缀
     * @author: XueYouPeng
     * @time: 23.8.2 下午 8:08
     */
    public static final String REDIS_ORDER_SECKILL = "seckill:goods:";

    /**
     * 秒杀活动的秒杀码数据前缀
     */
    public static final String REDIS_SECKILL_CODE_PRE = "seckill:code:" ;

    /**
     * 秒杀活动的缓存库存预热
     */
    public static final String REDIS_SECKILL_STOCK_COUNT = "seckill:stock:count:" ;

    /**
     * 秒杀订单的数据key
     */
    public static final String REDIS_SECKILL_ORDER = "seckill:order:" ;

}
