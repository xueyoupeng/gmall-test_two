package com.atstudent.gmall.common.properties;/*
 * @author: XueYouPeng
 * @time: 23.9.11 下午 2:26
 */

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "gmall.threadpool")
public class ThreadPoolProperties {

    private Integer     corePoolSize ;
    private Integer     maximumPoolSize ;
    private Integer   keepAliveTime ;
    private Integer       workQueueSize ;
}
