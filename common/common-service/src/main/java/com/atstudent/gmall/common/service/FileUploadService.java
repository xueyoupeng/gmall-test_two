package com.atstudent.gmall.common.service;/*
 * @author: XueYouPeng
 * @time: 23.7.11 下午 9:18
 */

import org.springframework.web.multipart.MultipartFile;

public interface FileUploadService {
    public abstract String upload(MultipartFile multipartFile);
}
