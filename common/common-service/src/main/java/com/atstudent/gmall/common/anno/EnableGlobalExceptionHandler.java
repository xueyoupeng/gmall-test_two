package com.atstudent.gmall.common.anno;/*
 * @author: XueYouPeng
 * @time: 23.7.11 下午 8:56
 */

import com.atstudent.gmall.common.exception.GlobalExceptionHandler;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(value = RetentionPolicy.RUNTIME)
@Target(value = ElementType.TYPE)
@Import(value = GlobalExceptionHandler.class)
public @interface EnableGlobalExceptionHandler {
}
