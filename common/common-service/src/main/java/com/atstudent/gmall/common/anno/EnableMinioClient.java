package com.atstudent.gmall.common.anno;/*
 * @author: XueYouPeng
 * @time: 23.9.5 下午 6:26
 */

import com.atstudent.gmall.common.config.MinioConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Retention(value = RetentionPolicy.RUNTIME)
@Target(value = ElementType.TYPE)
@Import(value = MinioConfiguration.class)
public @interface EnableMinioClient {
}
