package com.atstudent.gmall.common.anno;/*
 * @author: XueYouPeng
 * @time: 23.7.12 下午 7:13
 */

import com.atstudent.gmall.common.config.Swagger2Config;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(value = RetentionPolicy.RUNTIME)
@Target(value = ElementType.TYPE)
@Import(value = Swagger2Config.class)
public @interface EnableSwagger2Configuration {
}
