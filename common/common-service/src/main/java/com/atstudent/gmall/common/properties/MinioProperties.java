package com.atstudent.gmall.common.properties;/*
 * @author: XueYouPeng
 * @time: 23.7.12 下午 6:56
 */

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "gmall.minio")
public class MinioProperties {

    private String endpoint ;
    private String accessKey ;
    private String secretKey ;
    private String bucket ;
}
