package com.atstudent.gmall.common.anno;/*
 * @author: XueYouPeng
 * @time: 23.9.11 下午 2:36
 */

import com.atstudent.gmall.common.config.ThreadPoolExcutorConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(value = RetentionPolicy.RUNTIME)
@Target(value = ElementType.TYPE)
@Import(value = ThreadPoolExcutorConfiguration.class)
public @interface EnableThreadPool {
}
