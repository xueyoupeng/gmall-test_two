package com.atstudent.gmall.admin;/*
 * @author: XueYouPeng
 * @time: 23.9.7 下午 7:52
 */

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//声明当前这个微服务是一个sba的服务端
@EnableAdminServer
public class AdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(AdminApplication.class);
    }
}
