package com.atguigu.gmall.gateway.properties;/*
 * @author: XueYouPeng
 * @time: 23.9.13 下午 8:50
 */


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import java.util.List;

@Data
@ConfigurationProperties(prefix = "gmall.auth")
public class UserAuthProperties {

    private List<String> noauthurls ;
    private List<String>     authurls ;             // 需要验证用户登录的路径规则
    private String     toLoginPage ;
}
